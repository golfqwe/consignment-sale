//print contract
function downloadPdf() {

    var type = $('[name="type"]').find(":selected");
    var dateValue = $('[name="date"]').val();

    let textType = `${type.text()} `;
    switch (type.val()) {
        case 'date': textType += new Date(dateValue).toLocaleDateString('th-TH', { dateStyle: "long" })
            break;
        case 'range':
            let tempDate = dateValue.split(' to ')

            textType += `${new Date(tempDate[0]).toLocaleDateString('th-TH', { dateStyle: "long" })} ถึง ${new Date(tempDate[1]).toLocaleDateString('th-TH', { dateStyle: "long" })}`
            break;
        case 'month':
            let tempMonth = dateValue.split('-')
            textType += new Date(tempMonth[0], Number(tempMonth[1])-1).toLocaleDateString('th-TH', {
                year: 'numeric',
                month: 'long',
            })
            break;


    }

    let orders = [];

    $("table > tbody > tr").each(function () {
        orders.push([
            {
                text: $(this).find('td').eq(0).children().html(),
                alignment: 'center'
            },
            {
                text: $(this).find('td').eq(1).children().text(),
            },
            {
                text: $(this).find('td').eq(2).children().text(),
                alignment: 'right'
            },
            {
                text: $(this).find('td').eq(3).children().text(),
                alignment: 'right'
            },
            {
                text: $(this).find('td').eq(4).children().text(),
                alignment: 'right'
            },
            {
                text: $(this).find('td').eq(5).children().text(),
                alignment: 'right'
            },

        ])
    });


    this.pdfMake.fonts = {
        THSarabun: {
            normal: 'THSarabun.woff',
            bold: 'THSarabunBold.woff',
        }
    };
    var docDefinition = {
        defaultStyle: {
            font: 'THSarabun',
            fontSize: 14,
        },
        pageMargins: [30, 30, 30, 30],
        content: [
            { text: "รายงานสุรปยอดขาย", style: 'header', alignment: 'center' },
            { text: textType, style: 'subheader', alignment: 'center' },
            {
                style: 'tableExample',
                margin: [0, 10, 0, 0],
                table: {
                    widths: [20, '*', 70, 70, 70, 70],
                    headerRows: 1,
                    body: [
                        [
                            { text: "::", style: 'tableHeader', alignment: 'center' },
                            { text: "วันที่ / เดือนที่", style: 'tableHeader', alignment: 'center' },
                            { text: "ยอดจำนำ", style: 'tableHeader', alignment: 'center' },
                            { text: "ยอดต่อดอก", style: 'tableHeader', alignment: 'center' },
                            { text: "ยอดไถ่คืน", style: 'tableHeader', alignment: 'center' },
                            { text: "ยอดดอกเบี้ย", style: 'tableHeader', alignment: 'center' }
                        ],
                        ...orders
                    ]
                }
            },
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true
            },
            subheader: {
                fontSize: 16,
                bold: true
            },
            quote: {
                italics: true
            },
            small: {
                fontSize: 8
            },
            tableHeader: {
                bold: true,
                fontSize: 16,
                color: 'black'
            }
        }
    };
    pdfMake.createPdf(docDefinition).print();
}




const element1 = document.getElementById('datePicker');
const datePicker = flatpickr(element1, {

    maxDate: "today",
});

document.querySelector('#searchBy').addEventListener('change', function () {
    element1.value = "";
    switch (this.value) {
        case 'date':
            datePicker.config.mode = 'single';
            datePicker.config.dateFormat = "Y-m-d";
            break;
        case 'range':
            datePicker.config.mode = 'range';
            datePicker.config.dateFormat = "Y-m-d";
            break;
        case 'month':
            datePicker.config.mode = 'single';
            datePicker.config.dateFormat = "Y-m";
            break;
    }
});
