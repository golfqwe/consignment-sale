// JavaScript to handle modal open and close
document.getElementById('openModalBtn').addEventListener('click', function () {
    document.getElementById('myModal').classList.remove('hidden');
});

document.querySelectorAll('#closeModalBtn, #closeModalBtnBottom').forEach(button => {
    button.addEventListener('click', function () {
        document.getElementById('myModal').classList.add('hidden');
    });
});

const element = document.getElementById('choices');
const choices = new Choices(document.getElementById('choices'), {
    addItems: true,
    searchEnabled: true,
    searchChoices: true,
    itemSelectText: '',
    shouldSort: false,
    searchPlaceholderValue: 'ค้นหา',
});


// Listen for the search event
document.querySelector('#choices').addEventListener('search', (event) => {
    const input = event.detail.value;

    const hasNoResults = !choices._currentState.choices.some(item => item.label === input);

    if (hasNoResults) {
        // Display a message or handle the case where no results are found
        // Here, we're using a custom `noResultsText` to guide the user
        // choices.config.noResultsText = `No results found for "${input}". Press Enter to add.`;

        // Listen for the Enter key
        // Add keydown event listener to the Choices input
        const inputElement = document.querySelector('.choices__input--cloned');

        // Remove any previous keydown listener to prevent duplicate entries
        inputElement.removeEventListener('keydown', handleEnterKey);

        // Define the handleEnterKey function
        function handleEnterKey(event) {
            if (event.key === 'Enter') {
                event.preventDefault(); // Prevent form submission or default behavior
                // Add the new item
                choices.setChoices([{ value: input, label: input, selected: true }], 'value', 'label', false);

                // Reset the noResultsText
                choices.config.noResultsText = 'No results found';
            }
        }

        // Add the keydown listener to handle the Enter key press
        inputElement.addEventListener('keydown', handleEnterKey, { once: true });
    }

});


let camera_button = document.querySelector("#start-camera");
let video = document.querySelector("#video");
let click_button = document.querySelector("#click-photo");
let canvas = document.querySelector("#canvas");

let image_data_url = null

camera_button.addEventListener('click', async function () {
    canvas.classList.add('hidden')
    video.classList.remove('hidden')
    let stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
    video.srcObject = stream;
});

click_button.addEventListener('click', function () {
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    image_data_url = canvas.toDataURL('image/jpeg');
    video.classList.add('hidden')
    canvas.classList.remove('hidden')

    // data url of the image
    // console.log(image_data_url);
});


// date picker
flatpickr(document.getElementById('depositDate'), {});
flatpickr(document.getElementById('contractEndDate'), {});
