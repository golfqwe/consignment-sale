<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::index');
$routes->get('/getDashboard', 'Dashboard::getDashboard');
$routes->get('/customer', 'Customer::index');
$routes->get('/contract', 'Contract::index');

$routes->get('/orders', 'Orders::index');
$routes->get('/orderDetail/(:num)', 'Orders::getContract/$1');
$routes->get('/reports', 'Reports::index');

$routes->get('discount', 'Discount::index');
$routes->post('discount/save', 'Discount::save');
$routes->post('discount/update', 'Discount::update');
$routes->post('discount/delete', 'Discount::delete');

$routes->get('/interest_rate', 'InterestRate::index');
$routes->post('interest_rate/save', 'InterestRate::save');
$routes->post('interest_rate/update', 'InterestRate::update');
$routes->post('interest_rate/delete', 'InterestRate::delete');

$routes->get('/report_summary', 'ReportSummary::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
