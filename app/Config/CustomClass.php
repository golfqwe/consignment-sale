<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class CustomClass extends BaseConfig
{
    public $siteName  = 'Consignment Sale';
    public $siteEmail = 'webmaster@example.com';
}