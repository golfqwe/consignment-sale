<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>

<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">

  <?= $this->include('Layouts\navbar') ?>


  <?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
      <?php foreach ($errors as $field => $error): ?>
        <p><?= esc($error) ?></p>
      <?php endforeach ?>
    </div>
  <?php endif ?>

  <div class="w-full px-6 py-6 mx-auto">
    <div class="flex flex-wrap -mx-3">
      <div class="flex-none w-full max-w-full px-3">
        <div
          class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">
          <div class="p-6 pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
            <div class="flex flex-wrap -mx-3">
              <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                <h6>ข้อมูลลูกค้า</h6>
              </div>
              <div class="flex-none w-1/2 max-w-full px-3 text-right">
                <button id="btn-add"
                  class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                  <i
                    class="mr-2 far fa-plus bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                  เพิ่ม</button>
              </div>
            </div>
          </div>
          <div class="flex-auto px-0 pt-0 pb-2">
            <div class="p-0 overflow-x-auto">
              <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                <thead class="align-bottom">
                  <tr>
                    <!-- <th
                      class="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ::</th> -->
                    <th
                      class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      รหัสลูกค้า</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ชื่อ - นามสกุล</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      เลขประจำตัวประชาชน</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ที่อยู่</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      เบอร์โทร</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      รายละเอียดอื่นๆ</th>
                    <th
                      class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-gray-200 border-solid shadow-none tracking-none whitespace-nowrap text-slate-400 opacity-70">
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $index = 1;
                  foreach ($items as $item):
                    $checkLastItem = count($items) == $index ? '-0' : ''
                      ?>
                    <tr>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <div class="flex px-4 py-1">
                          <p class="mb-0 font-semibold leading-tight text-sm"><?php echo esc($item['id']); ?></p>
                        </div>
                      </td>
                      <!-- <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm"><?php echo esc($item['id']); ?></p>
                      </td> -->
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo esc($item['full_name']); ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo esc($item['personal_id']); ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?>  shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo esc($item['house_no'] . " " . $item['road'] . " " . $item['sub_district'] . " ". $item['district'] . " ". $item['province'] . " " . $item['post_code']); ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 text-sm font-semibold leading-tight"><?php echo esc($item['telephone']); ?></p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?>  shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center"><?php echo esc($item['mark']); ?>
                        </p>
                      </td>

                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <div class=" text-right">

                          <a class="btn-edit inline-block px-4 py-3 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 hover:scale-102 active:opacity-85 bg-x-25 text-slate-700"
                            data-id="<?php echo $item['id']; ?>" data-full_name="<?php echo $item['full_name']; ?>"
                            data-personal_id="<?php echo $item['personal_id']; ?>"
                            data-house_no="<?php echo $item['house_no']; ?>" data-road="<?php echo $item['road']; ?>"
                            data-sub_district="<?php echo $item['sub_district']; ?>"
                            data-district="<?php echo $item['district']; ?>"
                            data-province="<?php echo $item['province']; ?>"
                            data-post_code="<?php echo $item['post_code']; ?>"
                            data-telephone="<?php echo $item['telephone']; ?>" data-mark="<?php echo $item['mark']; ?>">
                            <i class="mr-2 fas fa-pencil-alt text-slate-700" aria-hidden="true"></i>Edit</a>
                        </div>
                      </td>
                    </tr>
                    <?php
                    $index++;
                  endforeach ?>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div id="myModalAdd"
    class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">
    <div class="bg-white rounded-lg overflow-hidden shadow-xl max-w-sm w-3/4 p-4">
      <div
        class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/4">
        <div class="flex flex-row items-center">
          <i class="mr-4 fas fa-plus"></i>
          <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">เพิ่มข้อมูลลูกค้า</h5>
        </div>
        <button id="closeModalAdd" class="text-gray-400 hover:text-gray-600">&times;</button>
      </div>

      <div class=" flex-auto px-4">
        <form action="/customer/save" method="post">
          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ชื่อ - นามสกุล</label>
              <div class="mb-4">
                <input type="text" name="full_name"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ชื่อ - นามสกุล" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เลขบัตรประจำตัวประชาชน</label>
              <div class="mb-4">
                <input type="text" name="personal_id"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="เลขบัตรประจำตัวประชาชน" aria-label="witness-2" aria-describedby="witness-2-addon" />
              </div>
            </div>
            <div class="basis-1/4">
            </div>
          </div>
          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">บ้านเลขที่</label>
              <div class="mb-4">
                <input type="text" name="house_no"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="บ้านเลขที่" aria-label="witness-2" aria-describedby="witness-2-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ซอย / ถนน</label>
              <div class="mb-4">
                <input type="text" name="road"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ซอย / ถนน" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ตำบล</label>
              <div class="mb-4">
                <input type="text" name="sub_district"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ตำบล" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">อำเภอ</label>
              <div class="mb-4">
                <select name="districtAdd"
                  class="districtAdd focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">
                </select>
                <!-- <input type="text" name="districtAdd"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="อำเภอ" aria-label="witness-1" aria-describedby="witness-1-addon" /> -->
              </div>
            </div>

          </div>

          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จังหวัด</label>
              <div class="mb-4">
                <select name="provinceAdd"
                  class="provinceAdd focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">
                </select>
                <!-- <input type="text" name="provinceAdd"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="จังหวัด" aria-label="witness-1" aria-describedby="witness-1-addon" /> -->
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">รหัสไปรษณีย์</label>
              <div class="mb-4">
                <input type="text" name="post_code"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="รหัสไปรษณีย์" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เบอร์โทรศัพท์</label>
              <div class="mb-4">
                <input type="text" name="telephone"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="เบอร์โทรศัพท์" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">หมายเหตุ</label>
              <div class="mb-4">
                <input type="text" name="mark"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="หมายเหตุ" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
          </div>
          <div class="text-right">
            <a href="#" id="closeModalAddBtn" class="px-4 py-2 text-xs bg-gray-500  rounded ">ปิด</a>
            <button type="submit" data-toggle="modal" data-target="#import"
              class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">เพิ่ม</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal  myModalEdit-->
  <div id="myModalEdit"
    class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">
    <div class="bg-white rounded-lg  shadow-xl max-w-sm w-3/4 p-4">
      <div
        class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/4">
        <div class="flex flex-row items-center">
          <i class="mr-4 fas fa-pencil-alt"></i>
          <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">แก้ไขข้อมูลลูกค้า</h5>
        </div>
        <a href="#" id="closeModalEdit" class="text-gray-400 hover:text-gray-600">&times;</a>
      </div>

      <div class=" flex-auto px-4">
        <form action="/customer/update" method="post">
          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ชื่อ - นามสกุล</label>
              <div class="mb-4">
                <input type="text" name="full_name"
                  class="full_name focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ชื่อ - นามสกุล" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เลขบัตรประจำตัวประชาชน</label>
              <div class="mb-4">
                <input type="text" name="personal_id"
                  class="personal_id focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="เลขบัตรประจำตัวประชาชน" aria-label="witness-2" aria-describedby="witness-2-addon" />
              </div>
            </div>
            <div class="basis-1/4">
            </div>
          </div>
          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">บ้านเลขที่</label>
              <div class="mb-4">
                <input type="text" name="house_no"
                  class="house_no focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="บ้านเลขที่" aria-label="witness-2" aria-describedby="witness-2-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ซอย / ถนน</label>
              <div class="mb-4">
                <input type="text" name="road"
                  class="road focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ซอย / ถนน" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ตำบล</label>
              <div class="mb-4">
                <input type="text" name="sub_district"
                  class="sub_district focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ตำบล" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">อำเภอ</label>
              <div class="mb-4">
                <select name="district"
                  class="district focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none">
                </select>
                <!-- <input type="text" name="district"
                  class="district focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="อำเภอ" aria-label="witness-1" aria-describedby="witness-1-addon" /> -->
              </div>
            </div>

          </div>

          <div class="flex flex-row space-x-4">
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จังหวัด</label>
              <div class="mb-4">
                <select name="province"
                  class="province focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">
                </select>
                <!-- <input type="text" name="province"
                  class="province focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="จังหวัด" aria-label="witness-1" aria-describedby="witness-1-addon" /> -->
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">รหัสไปรษณีย์</label>
              <div class="mb-4">
                <input type="text" name="post_code"
                  class="post_code focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="รหัสไปรษณีย์" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เบอร์โทรศัพท์</label>
              <div class="mb-4">
                <input type="text" name="telephone"
                  class="telephone focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="เบอร์โทรศัพท์" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
            <div class="basis-1/4">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">หมายเหตุ</label>
              <div class="mb-4">
                <input type="text" name="mark"
                  class="mark focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="หมายเหตุ" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>
          </div>
          <div class="text-right">
            <input type="hidden" name="customer_id" class="customer_id" />
            <a href="#" id="closeModalEditBtn" class="px-4 py-2 text-xs bg-gray-500  rounded ">ปิด</a>
            <button type="submit"
              class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">บันทึก</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <script>
    $(document).ready(async function () {

      let provinces = [];
      let districts = [];
      
      try {
        const items = await fetch('https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province.json');
        let list = await items.json();
        provinces = list.map(it => ({ value: it.name_th, label: it.name_th }));
      } catch (err) {
        console.error(err);
      }

      try {
        const items = await fetch('https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_amphure.json');
        let list = await items.json();
        districts = list.map(it => ({ value: it.name_th, label: it.name_th }));
      } catch (err) {
        console.error(err);
      }

      const elProvince = new Choices(document.querySelector('[name="province"]'), { searchPlaceholderValue: 'ค้นหา' });
      const elProvinceAdd = new Choices(document.querySelector('[name="provinceAdd"]'), { searchPlaceholderValue: 'ค้นหา' });
      elProvince.setChoices(() => provinces);
      elProvinceAdd.setChoices(() => provinces);

      const elDistrict = new Choices(document.querySelector('[name="district"]'), { searchPlaceholderValue: 'ค้นหา' });
      const elDistrictAdd = new Choices(document.querySelector('[name="districtAdd"]'), { searchPlaceholderValue: 'ค้นหา' });
      elDistrict.setChoices(() => districts);
      elDistrictAdd.setChoices(() => districts);

      // get add discount
      $('#btn-add').on('click', function () {
        $('#myModalAdd').removeClass('hidden');

      });
      $('#closeModalAdd,#closeModalAddBtn').on('click', function () {
        $('#myModalAdd').addClass('hidden');
      })

      // get Edit discount
      $('.btn-edit').on('click', function () {

        const id = $(this).data('id');
        const full_name = $(this).data('full_name');
        const personal_id = $(this).data('personal_id');
        const house_no = $(this).data('house_no');
        const road = $(this).data('road');
        const sub_district = $(this).data('sub_district');
        const district = $(this).data('district');
        console.log("🚀 ~ district:", district)
        const province = $(this).data('province');
        console.log("🚀 ~ province:", province)
        const post_code = $(this).data('post_code');
        const telephone = $(this).data('telephone');
        const mark = $(this).data('mark');


        // Set data to Form Edit
        $('.customer_id').val(id);
        $('.full_name').val(full_name);
        $('.personal_id').val(personal_id);
        $('.house_no').val(house_no);
        $('.road').val(road);
        $('.sub_district').val(sub_district);
        // $('[name="district"]').val(district);
        elDistrict.setChoiceByValue(district);
        $('.province').val(province);
        elProvince.setChoiceByValue(province);
        $('.post_code').val(post_code);
        $('.telephone').val(telephone);
        $('.mark').val(mark);

        $('#myModalEdit').removeClass('hidden');
      })

      $('#closeModalEdit,#closeModalEditBtn').on('click', function () {
        $('#myModalEdit').addClass('hidden');
      })

    })

  </script>

</main>




<?= $this->endSection() ?>