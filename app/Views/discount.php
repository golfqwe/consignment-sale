<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>

<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">

  <?= $this->include('Layouts\navbar') ?>


  <?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
      <?php foreach ($errors as $field => $error): ?>
        <p><?= esc($error) ?></p>
      <?php endforeach ?>
    </div>
  <?php endif ?>

  <div class="w-full px-6 py-6 mx-auto">
    <div class="flex flex-wrap -mx-3">
      <div class="flex-none w-full max-w-full px-3">
        <div
          class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">
          <div class="p-6 pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
            <div class="flex flex-wrap -mx-3">
              <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                <h6>อัตราส่วนลด (ร้อยละ)</h6>
              </div>
              <div class="flex-none w-1/2 max-w-full px-3 text-right">
                <button id="btn-add"
                  class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                  <i
                    class="mr-2 far fa-plus bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                  เพิ่ม</button>
              </div>
            </div>
          </div>
          <div class="flex-auto px-0 pt-0 pb-2">
            <div class="p-0 overflow-x-auto">
              <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                <thead class="align-bottom">
                  <tr>
                    <th
                      class="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ::</th>
                    <th
                      class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      จำนวนวัน</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ร้อยละ %</th>
                    <!-- <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      สถานะ</th> -->
                    <th
                      class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-gray-200 border-solid shadow-none tracking-none whitespace-nowrap text-slate-400 opacity-70">
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $index = 1;
                  foreach ($items as $item):
                    $checkLastItem = count($items) == $index ? '-0' : ''
                      ?>
                    <tr>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <div class="flex px-4 py-1">
                          <p class="mb-0 font-semibold leading-tight text-sm"><?php echo esc($item['id']); ?></p>
                        </div>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?>  whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm"><?php echo esc($item['conunt_date']); ?></p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?>  whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo esc($item['percent']); ?> %
                        </p>
                      </td>
                      <!-- <td
                        class="p-2 leading-normal text-center align-middle bg-transparent border-b text-sm whitespace-nowrap shadow-transparent">
                        <span
                          class="bg-gradient-to-tl from-green-600 to-lime-400 px-3.6 text-sm rounded-1.8 py-2.2 inline-block whitespace-nowrap text-center align-baseline font-bold uppercase leading-none text-white">
        
                        </span>
                      </td> -->

                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?>  whitespace-nowrap shadow-transparent">
                        <div class=" text-right">
                          <a class="btn-delete relative z-10 inline-block px-4 py-3 mb-0 font-bold text-center text-transparent uppercase align-middle transition-all border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 bg-gradient-to-tl from-red-600 to-rose-400 hover:scale-102 active:opacity-85 bg-x-25 bg-clip-text"
                            data-id="<?php echo $item['id']; ?>"><i
                              class="mr-2 far fa-trash-alt bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>Delete</a>
                          <a class="btn-edit inline-block px-4 py-3 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 hover:scale-102 active:opacity-85 bg-x-25 text-slate-700"
                            data-id="<?php echo $item['id']; ?>" data-countdate="<?php echo $item['conunt_date']; ?>"
                            data-percent=" <?php echo $item['percent']; ?>"><i
                              class="mr-2 fas fa-pencil-alt text-slate-700" aria-hidden="true"></i>Edit</a>
                        </div>
                      </td>
                    </tr>
                    <?php
                    $index++;
                  endforeach ?>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal add-->
  <div id="myModalAdd"
    class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">

    <form action="/discount/save" method="post">
      <div class="bg-white rounded-lg overflow-hidden shadow-xl max-w-sm w-2/4 p-4">
        <div
          class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/4">
          <div class="flex flex-row items-center">
            <i class="mr-4 fas fa-plus"></i>
            <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">เพิ่มอัตราส่วนลด (ร้อยละ)</h5>
          </div>
          <a href="#" id="closeModalAdd" class="text-gray-400 hover:text-gray-600">&times;</a>
        </div>

        <div class=" flex-auto px-4">
          <div class="flex flex-row space-x-4">
            <div class="basis-1/2">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จำนวนวัน</label>
              <div class="mb-4">
                <input type="number"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="จำนวนวัน" name="countDate" aria-label="contractNo" aria-describedby="contractNo-addon" />
              </div>
            </div>
            <div class="basis-1/2">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ร้อยละ</label>
              <div class="mb-4">
                <input type="text"
                  class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ร้อยละ" name="percent" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>

          </div>

          <div class="flex flex-row space-x-4 justify-end">
            <div class=" text-right mt-6">
              <a href="#" id="closeModalAddBtn" class="px-4 py-2 text-xs bg-gray-500  rounded ">ปิด</a>
              <button type="submit"
                class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">บันทึก</button>
            </div>
          </div>

        </div>

      </div>
    </form>
  </div>

  <!-- Modal edit-->
  <div id="myModalEdit"
    class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">

    <form action="/discount/update" method="post">
      <div class="bg-white rounded-lg overflow-hidden shadow-xl max-w-sm w-2/4 p-4">
        <div
          class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/4">
          <div class="flex flex-row items-center">
            <i class="mr-4 fas fa-pencil-alt"></i>
            <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">แก้ไขอัตราส่วนลด (ร้อยละ)</h5>
          </div>
          <a href="#" id="closeModalEdit" class="text-gray-400 hover:text-gray-600">&times;</a>
        </div>

        <div class=" flex-auto px-4">
          <div class="flex flex-row space-x-4">
            <div class="basis-1/2">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จำนวนวัน</label>
              <div class="mb-4">
                <input type="number"
                  class="countDate focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="จำนวนวัน" name="countDate" aria-label="contractNo" aria-describedby="contractNo-addon" />
              </div>
            </div>
            <div class="basis-1/2">
              <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ร้อยละ</label>
              <div class="mb-4">
                <input type="text"
                  class="percent focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                  placeholder="ร้อยละ" name="percent" aria-label="witness-1" aria-describedby="witness-1-addon" />
              </div>
            </div>

          </div>

          <div class="flex flex-row space-x-4 justify-end">
            <input type="hidden" name="discount_id" class="discount_id" />
            <div class=" text-right mt-6">
              <a href="#" id="closeModalEditBtn" class="px-4 py-2 text-xs bg-gray-500  rounded ">ปิด</a>
              <button type="submit"
                class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">บันทึก</button>
            </div>
          </div>

        </div>

      </div>
    </form>
  </div>

  <!-- Modal Delete Product-->
  <div id="myModalDelete"
    class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">
    <form action="/discount/delete" method="post">
      <div class="bg-white rounded-lg overflow-hidden shadow-xl max-w-sm w-2/4 p-4">
        <div
          class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/4">
          <div class="flex flex-row items-center">
            <i class="mr-4 fas fa-trash"></i>
            <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">ลบอัตราส่วนลด</h5>
          </div>
          <a href="#" id="closeModalDel" class="text-gray-400 hover:text-gray-600">&times;</a>
        </div>
        <div class=" flex-auto p-4">
          <div class="flex flex-row space-x-4">
            <div class="basis">
              <h5>คุณต้องการลบข้อมูลนี้ใช่หรือไม่?</h5>
            </div>

          </div>

          <div class="flex flex-row space-x-4 justify-end">
            <input type="hidden" name="discount_id" class="discount_id" />
            <div class=" text-right mt-6">
              <a href="#"  id="closeModalDelBtn"
                class="px-4 py-2 text-xs bg-gray-500  rounded ">ปิด</a>
              <button type="submit"
                class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">บันทึก</button>
            </div>
          </div>

        </div>
      </div>
    </form>
  </div>
  <!-- End Modal Delete Product-->



</main>

<script>
  $(document).ready(function () {


    // get Edit discount
    $('.btn-edit').on('click', function () {
      const id = $(this).data('id');
      const countDate = $(this).data('countdate');
      const percent = $(this).data('percent');

      // Set data to Form Edit
      $('.discount_id').val(id);
      $('.countDate').val(countDate);
      $('.percent').val(percent);

      $('#myModalEdit').removeClass('hidden');
    })

    $('#closeModalEdit,#closeModalEditBtn').on('click', function () {
      $('#myModalEdit').addClass('hidden');
    })

    // get add discount
    $('#btn-add').on('click', function () {
      $('#myModalAdd').removeClass('hidden');
    })
    $('#closeModalAdd,#closeModalAddBtn').on('click', function () {
      $('#myModalAdd').addClass('hidden');
    })

    // get delet discount
    $('.btn-delete').on('click', function () {
      const id = $(this).data('id');
      $('.discount_id').val(id);
      $('#myModalDelete').removeClass('hidden');
    })
    $('#closeModalDel,#closeModalDelBtn').on('click', function () {
      $('#myModalDelete').addClass('hidden');
    })

  })




  // Optional: close modal when clicking outside of it
  window.addEventListener('click', function (event) {
    if (event.target == document.getElementById('myModal')) {
      document.getElementById('myModal').classList.add('hidden');
    }
  });

</script>


<?= $this->endSection() ?>