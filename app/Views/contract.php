<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>


<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">
    <?= $this->include('Layouts\navbar') ?>



    <div class="w-full px-6 py-6 mx-auto">
        <?php if (!empty($errors)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errors as $field => $error): ?>
                    <p><?= esc($error) ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>

        <div class="flex flex-wrap -mx-3">
            <div class="flex-none w-full max-w-full px-3">
                <div
                    class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border p-4">

                    <form id="formContract">
                        <div class="pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                            <h6>ข้อมูลสัญญา</h6>
                        </div>
                        <div class="flex-auto px-4">

                            <div class="flex flex-row space-x-4">
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เลขที่สัญญา <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="text" required readonly name="contract_id"
                                            value="<?php echo date_format(date_create(), "Ymd"); ?>C<?php echo str_pad($maxid + 1, 8, "0", STR_PAD_LEFT); ?>"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เลขที่สัญญา" aria-label="contractNo"
                                            aria-describedby="contractNo-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">พยาน 1</label>
                                    <div class="mb-4">
                                        <input type="text" name="witness1"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เลขที่สัญญา" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">พยาน 2</label>
                                    <div class="mb-4">
                                        <input type="text" name="witness2"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เลขที่สัญญา" aria-label="witness-2"
                                            aria-describedby="witness-2-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">สถานที่เก็บ</label>
                                    <div class="mb-4">
                                        <input type="text" name="storage_location"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เลขที่สัญญา" aria-label="location"
                                            aria-describedby="location-addon" />
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-row space-x-4 ">
                                <!-- <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จำนวน <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="number" required name="quantity"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="จำนวน" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div> -->
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ยอดฝาก <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="text" required name="deposit_amount"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ยอดฝาก" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">อัตราดอกเบี้ย <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <select required id="interest_rate" name="interest_rate"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">
                                            <option selected></option>

                                            <?php foreach ($interestRate as $iterRate): ?>
                                                <option
                                                    data-value='{"conunt_date":<?php echo $iterRate['conunt_date'] ?>,"percent":<?php echo $iterRate['percent'] ?>}'>
                                                    <?php echo $iterRate['conunt_date'] ?> วัน
                                                    <?php echo $iterRate['percent'] ?> %
                                                </option>
                                            <?php endforeach ?>

                                        </select>

                                    </div>
                                </div>
                                <!-- <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค่าธรรมเนียม <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="number" required name="fee"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ค่าธรรมเนียม" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div> -->
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ยอดจ่าย <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="text" required name="total"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ยอดจ่าย" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-row space-x-4 ">
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">วันที่ฝาก <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input id="depositDate" required name="deposit_date"
                                            class="focus:shadow-soft-primary-outline dark:bg-gray-950 dark:placeholder:text-white/80 dark:text-white/80 text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none"
                                            type="text" placeholder="วันที่ฝาก" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">วันที่สิ้นสุดสัญญา <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input id="contractEndDate" required name="contract_end_date"
                                            class="focus:shadow-soft-primary-outline dark:bg-gray-950 dark:placeholder:text-white/80 dark:text-white/80 text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none"
                                            type="text" placeholder="วันที่สิ้นสุดสัญญา" />
                                    </div>
                                </div>
                                <div class="basis-1/4"></div>
                                <div class="basis-1/4"></div>
                            </div>


                        </div>

                        <div
                            class="pb-0 mb-0 mt-4 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                            <h6>ข้อมูลผู้ขายฝาก</h6>
                        </div>
                        <div class="flex-auto px-4">
                            <div class="flex flex-row space-x-4">
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค้นหา <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">

                                        <select id="choices" name="custome[search_id]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none">
                                            <option selected value=""> กรุณาเลือก</option>
                                            <?php foreach ($customers as $customer): ?>
                                                <option value='{ "personal_id": "<?php echo $customer['personal_id']; ?>", 
                                                        "full_name": "<?php echo $customer['full_name']; ?>", 
                                                              
                                                              "house_no": "<?php echo $customer['house_no']; ?>",
                                                               "road": "<?php echo $customer['road']; ?>", 
                                                               "sub_district": "<?php echo $customer['sub_district']; ?>",
                                                               "district": "<?php echo $customer['district']; ?>", 
                                                               "province": "<?php echo $customer['province']; ?>", 
                                                               "post_code": "<?php echo $customer['post_code']; ?>", 
                                                               "telephone": "<?php echo $customer['telephone']; ?>", 
                                                               "mark": "<?php echo $customer['mark']; ?>",
                                                               "id": "<?php echo $customer['id']; ?>"
                                                             }'>
                                                    <?php echo $customer['personal_id']; ?> (
                                                    <?php echo $customer['full_name']; ?> )
                                                </option>

                                            <?php endforeach ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ชื่อ - นามสกุล <span
                                            class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="hidden" name="custome[id]" />
                                        <input type="text" required name="custome[full_name]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ชื่อ - นามสกุล" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เลขบัตรประจำตัวประชาชน
                                        <span class="text-red-600">*</span></label>
                                    <div class="mb-4">
                                        <input type="text" required name="custome[personal_id]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เลขบัตรประจำตัวประชาชน" aria-label="witness-2"
                                            aria-describedby="witness-2-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                </div>
                            </div>
                            <div class="flex flex-row space-x-4">
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">บ้านเลขที่</label>
                                    <div class="mb-4">
                                        <input type="text" name="custome[house_no]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="บ้านเลขที่" aria-label="witness-2"
                                            aria-describedby="witness-2-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ซอย / ถนน</label>
                                    <div class="mb-4">
                                        <input type="text" name="custome[road]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ซอย / ถนน" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ตำบล</label>
                                    <div class="mb-4">
                                        <!-- <select id="choicesSubDistrict" name="custome[sub_district]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none">
                                        </select> -->
                                        <input type="text" name="custome[sub_district]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="ตำบล" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">อำเภอ</label>
                                    <div class="mb-4">
                                        <select id="choicesDistrict" name="custome[district]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none">
                                        </select>
                                        <!-- <input type="text" name="custome[district]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="อำเภอ" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" /> -->
                                    </div>
                                </div>

                            </div>

                            <div class="flex flex-row space-x-4">
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">จังหวัด</label>
                                    <div class="mb-4">
                                        <select id="choicesProvince" name="custome[province]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none">
                                        </select>
                                        <!-- <input type="text" name="custome[province]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="จังหวัด"  /> -->
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">รหัสไปรษณีย์</label>
                                    <div class="mb-4">
                                        <input type="text" name="custome[post_code]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="รหัสไปรษณีย์" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เบอร์โทรศัพท์</label>
                                    <div class="mb-4">
                                        <input type="text" name="custome[telephone]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="เบอร์โทรศัพท์" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                                <div class="basis-1/4">
                                    <label class="mb-2 ml-1 font-bold text-xs text-slate-700">หมายเหตุ</label>
                                    <div class="mb-4">
                                        <input type="text" name="custome[mark]"
                                            class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                            placeholder="หมายเหตุ" aria-label="witness-1"
                                            aria-describedby="witness-1-addon" />
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="text-center">
                                <button type="button"
                                    class="inline-block w-full px-6 py-3 mt-6 mb-0 font-bold text-center text-white uppercase align-middle transition-all bg-transparent border-0 rounded-lg cursor-pointer shadow-soft-md bg-x-25 bg-150 leading-pro text-xs ease-soft-in tracking-tight-soft bg-gradient-to-tl from-blue-600 to-cyan-400 hover:scale-102 hover:shadow-soft-xs active:opacity-85">Sign
                                    in</button>
                            </div> -->
                        </div>

                        <div
                            class="pb-0 mb-0 mt-4 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">

                            <div class="flex flex-wrap -mx-3">
                                <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                                    <h6>ข้อมูลสินค้าขายฝาก</h6>
                                </div>
                                <div class="flex-none w-1/2 max-w-full px-3 text-right">
                                    <a href="#" id="openModalBtn"
                                        class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                                        <i
                                            class="mr-2 far fa-plus bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                                        เพิ่ม</a>
                                </div>
                            </div>
                        </div>
                        <div class="flex-auto px-0 pt-0 pb-2">
                            <div class="p-0 overflow-x-auto">
                                <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                                    <thead class="align-bottom">
                                        <tr>
                                            <th
                                                class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                สินค้า</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                ยี่ห้อ / รุ่น</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                ขนาด</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                น้ำหนัก</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                สี</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                หมายเลขสินค้า</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                หมายเหตุ</th>
                                            <th
                                                class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                                รูป</th>
                                            <th
                                                class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-gray-200 border-solid shadow-none tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="flex  justify-center space-x-4">
                            <a onclick="window.location.reload();"
                                class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                                <i
                                    class="mr-2 fa fa-refresh bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                                เริ่มรายการใหม่</a>
                            <a onclick="downloadPdf()" disabled id="btnPrint"
                                class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none  leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                                <i
                                    class="mr-2 fa fa-print bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                                พิมพ์ใบสัญญา</a>
                            <button type="submit"
                                class="inline-block px-8 py-2 m-1 font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl text-xs from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">
                                <i
                                    class="mr-2 far fa-save bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                                บันทึก</button>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div id="myModal"
        class="fixed top-0 left-0 flex items-center justify-center bg-black bg-opacity-50 z-sticky w-full h-full overflow-x-hidden overflow-y-auto transition-opacity hidden">
        <div class="bg-white rounded-lg overflow-hidden shadow-xl max-w-sm w-3/4 p-4">
            <!-- <div class="flex justify-between items-center">
                <div>
                    <i class="mr-4 fas fa-plus"></i>
                    <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">เพิ่มสินค้า</h5>
                </div>
                <button id="closeModalBtn" class="text-gray-400 hover:text-gray-600">&times;</button>
            </div> -->
            <div
                class="flex items-center justify-between p-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl basis-3/6">
                <div class="flex flex-row items-center">
                    <i class="mr-4 fas fa-plus"></i>
                    <h5 class="mb-0 leading-normal dark:text-white" id="ModalLabel">เพิ่มสินค้า</h5>
                </div>
                <button id="closeModalBtn" class="text-gray-400 hover:text-gray-600">&times;</button>
            </div>

            <div class=" flex-auto px-4">
                <form id="formProduct" accept-charset="multipart/form-data">
                    <div class="flex flex-row space-x-4">
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">สินค้าขายฝาก <span
                                    class="text-red-600">*</span></label>
                            <div class="mb-4">
                                <input type="text" required name="name"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="สินค้าขายฝาก" aria-label="contractNo"
                                    aria-describedby="contractNo-addon" />
                            </div>
                        </div>
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ยี่ห้อ / รุ่น</label>
                            <div class="mb-4">
                                <input type="text" name="brand_model"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="ยี่ห้อ / รุ่น" aria-label="witness-1"
                                    aria-describedby="witness-1-addon" />
                            </div>
                        </div>
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ขนาด</label>
                            <div class="mb-4">
                                <input type="text" name="size"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="ขนาด" aria-label="witness-2" aria-describedby="witness-2-addon" />
                            </div>
                        </div>
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">น้ำหนัก</label>
                            <div class="mb-4">
                                <input type="text" name="weight"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="น้ำหนัก" aria-label="witness-2" aria-describedby="witness-2-addon" />
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-row space-x-4">
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">สี</label>
                            <div class="mb-4">
                                <input type="text" name="color"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="สี" aria-label="witness-2" aria-describedby="witness-2-addon" />
                            </div>
                        </div>
                        <div class="basis-1/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">หมายเลขสินค้า</label>
                            <div class="mb-4">
                                <input type="text" name="serial_number"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="หมายเลขสินค้า" aria-label="witness-1"
                                    aria-describedby="witness-1-addon" />
                            </div>
                        </div>
                        <div class="basis-2/4">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700">หมายเหตุ</label>
                            <div class="mb-4">
                                <input type="text" name="mark"
                                    class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                                    placeholder="หมายเหตุ" aria-label="witness-1" aria-describedby="witness-1-addon" />
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-row space-x-4 ">
                        <div class="basis-1/4">

                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700" for="file_input">Upload
                                file</label>

                            <input type="file" name="imageUpload" id="imageUpload" accept="image/*"
                                class="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100 " />

                        </div>
                        <div class="basis-1/4  flex-col text-center space-y-4">

                            <div class="flex flex-col ">
                                <a id="start-camera"
                                    class=" px-8 py-2 mb-2 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">Start
                                    Camera</a>
                                <video id="video" width="320" height="240" autoplay class="hidden block"></video>
                                <canvas id="canvas" name="imageCanvas" width="300" height="220"
                                    class="hidden block"></canvas>
                                <a id="click-photo"
                                    class=" px-8 py-2 mt-2 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">Click
                                    Photo</a>
                            </div>
                        </div>
                        <div class="basis-1/4  flex-col text-center space-y-4"></div>
                        <div class="basis-1/4 text-right mt-6">
                            <button id="closeModalBtnBottom" class="px-4 py-2 bg-gray-500 text-xs rounded ">ปิด</button>
                            <button type="submit"
                                class="inline-block px-8 py-2 m-1 text-xs font-bold text-center text-white uppercase align-middle transition-all border-0 rounded-lg cursor-pointer ease-soft-in leading-pro tracking-tight-soft bg-gradient-to-tl from-purple-700 to-pink-500 shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85">เพิ่ม</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <script src="<?php echo base_url() ?>/assets/js/pdfmake/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/pdfmake/th-sarabun.js"></script>

    <script src="<?php echo base_url() ?>/assets/js/page/contract.js" async></script>
    <script src="<?php echo base_url() ?>/assets/js/page/printOutContract.js" async></script>

    <script>

        let dataPrintout = {};
        const elProvince = new Choices(document.querySelector('[name="custome\[province\]"]'), { searchPlaceholderValue: 'ค้นหา' });

        elProvince.setChoices(async () => {
            try {
                const items = await fetch('https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province.json');
                let list = await items.json();
                return list.map(it => ({ value: it.name_th, label: it.name_th }));
            } catch (err) {
                console.error(err);
            }
        });
        const elDistrict = new Choices(document.querySelector('[name="custome\[district\]"]'), { searchPlaceholderValue: 'ค้นหา' });

        elDistrict.setChoices(async () => {
            try {
                const items = await fetch('https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_amphure.json');
                let list = await items.json();
                return list.map(it => ({ value: it.name_th, label: it.name_th }));
            } catch (err) {
                console.error(err);
            }
        });



        new Cleave('[name="deposit_amount"]', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        new Cleave('[name="total"]', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });

        let current = new Date();
        $('[name="deposit_date"]').val(current.toISOString().substring(0, 10))


        const convertBase64 = (file) => {
            return new Promise((resolve, reject) => {
                const fileReader = new FileReader();
                fileReader.readAsDataURL(file);

                fileReader.onload = () => {
                    resolve(fileReader.result);
                };

                fileReader.onerror = (error) => {
                    reject(error);
                };
            });
        };

        const deleteProduct = (id) => {
            $('table tbody tr#'+id).remove()
        }

        let lineNo = 1;

        $("#formProduct").submit(async function (e) {
            e.preventDefault();
            var fields = $(":input").serializeArray();

            var file = $('#imageUpload').prop('files');

            if (file.length) {
                image_data_url = await convertBase64(file[0])
            }

            mapObject = {};
            fields.forEach(it => {
                mapObject[it.name] = it.value
            })


            let markup = `<tr id="${lineNo}">`;
            markup += ` 
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm">${mapObject?.name}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm">${mapObject?.brand_model || '-'}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm">${mapObject?.size || '-'}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm text-center">${mapObject?.weight || '-'}
                    </p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm text-center">${mapObject?.color || '-'}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm text-center">${mapObject?.serial_number || '-'}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <p class="mb-0 font-semibold leading-tight text-sm text-center">${mapObject?.mark || '-'}</p>
                </td>
                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    
                    ${image_data_url ? ` <img src="${image_data_url}" alt="img-blur-shadow" width="125" class=" shadow-soft-2xl rounded-2xl" />` : '<p class="mb-0 font-semibold leading-tight text-sm text-center">-</p>'}
                   
                </td>

                <td
                    class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                    <div class=" text-right">
                        <a onclick="deleteProduct(${lineNo})" class="relative z-10 inline-block px-4 py-3 mb-0 font-bold text-center text-transparent uppercase align-middle transition-all border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 bg-gradient-to-tl from-red-600 to-rose-400 hover:scale-102 active:opacity-85 bg-x-25 bg-clip-text"
                           data-id="${lineNo}"><i
                                class="mr-2 far fa-trash-alt bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>Delete</a>
                    </div>
                </td>
                `
            markup += "</tr>";

            tableBody = $("table tbody");
            tableBody.append(markup);

            lineNo++;
            $('#myModal').addClass('hidden');
        })

        let interestRate = 0;

        $("#interest_rate").change(function () {
            const deposit_amount = $('[name="deposit_amount"]');
            interestRate = $(this).find(":selected").data("value")
            let deposit = Number(deposit_amount.val().replaceAll(',', ''))
            if (deposit) {
                const price = (deposit * interestRate.percent / 100) + deposit
                $('[name="total"]').val(new Intl.NumberFormat().format(price))
            }

            const deposit_date = $('[name="deposit_date"]');
            var contractEndDate = new Date(deposit_date.val());

            // add a day
            contractEndDate.setDate(contractEndDate.getDate() + interestRate.conunt_date);
            $('[name="contract_end_date"]').val(contractEndDate.toISOString().substring(0, 10))

        });

        $('[name="custome\[search_id\]"]').change(function () {
            const customerStr = $(this).find(":selected").val();
            if (customerStr == "") {
                $('[name="custome\[id]"\]').val('');
                $('[name="custome\[full_name]"\]').val('');
                $('[name="custome\[personal_id]"\]').val('');
                $('[name="custome\[house_no]"\]').val('');
                $('[name="custome\[road]"\]').val('');
                $('[name="custome\[sub_district]"\]').val('');
                $('[name="custome\[district]"\]').val('');
                $('[name="custome\[province]"\]').val('');
                $('[name="custome\[post_code]"\]').val('');
                $('[name="custome\[telephone]"\]').val('');
                $('[name="custome\[mark]"\]').val('');
                return
            }
            const customer = JSON.parse(customerStr);

            $('[name="custome\[id]"\]').val(customer.id);
            $('[name="custome\[full_name]"\]').val(customer.full_name);
            $('[name="custome\[personal_id]"\]').val(customer.personal_id);
            $('[name="custome\[house_no]"\]').val(customer.house_no);
            $('[name="custome\[road]"\]').val(customer.road);
            $('[name="custome\[sub_district]"\]').val(customer.sub_district);
            // $('[name="custome\[district]"\]').val(customer.district);
            // $('[name="custome\[province]"\]').val(customer.province);
            elDistrict.setChoiceByValue(customer.district);
            elProvince.setChoiceByValue(customer.province);
            $('[name="custome\[post_code]"\]').val(customer.post_code);
            $('[name="custome\[telephone]"\]').val(customer.telephone);
            $('[name="custome\[mark]"\]').val(customer.mark);


        });

        $("#formContract").submit(function (e) {
            e.preventDefault();
            let contract = $('#formContract').serializeJSON();
            delete contract.custome.search_id
            delete interest_rate

            contract['contract_id'] = Number(contract.contract_id.substring(11));
            contract['deposit_amount'] = Number(contract.deposit_amount.replaceAll(',', ''))
            contract['interest_day'] = interestRate.conunt_date;
            contract['interest_rate'] = interestRate.percent;
            contract['total'] = parseFloat(contract.total.replaceAll(',', ''));
            console.log("🚀 ~ contract:", contract)

            let products = []
            $("table > tbody > tr").each(function () {
                products.push({
                    name: $(this).find('td').eq(0).children().text(),
                    brand_model: $(this).find('td').eq(1).children().text(),
                    size: $(this).find('td').eq(2).children().text(),
                    weight: parseFloat($(this).find('td').eq(3).children().text()) || 0,
                    color: $(this).find('td').eq(4).children().text(),
                    serial_number: $(this).find('td').eq(5).children().text(),
                    mark: $(this).find('td').eq(6).children().text(),
                    image: $(this).find('td').eq(7).children().attr('src'),
                })
            });

            contract['quantity'] = products.length

            dataPrintout = { ...contract, products };
            $.ajax({
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                url: "/contract/save",
                type: "post",
                data: JSON.stringify({ ...contract, products }),
                contentType: "application/json; charset=utf-8",
                success: async function (response) {

                    dataPrintout.custome.id = response.id
                    await Swal.fire({
                        title: 'Success',
                        text: 'บันทึกข้อมูลสำเร็จ',
                        icon: 'success',
                    })


                 $("#btnPrint").prop('disabled', false);
                 $("#btnPrint").addClass('cursor-pointer')

                 downloadPdf()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        title: 'Error!',
                        text: 'มีบางอย่างผิดพลาด',
                        icon: 'error',
                        confirmButtonText: 'close'
                    })
                }
            });

        })
    </script>

</main>






<?= $this->endSection() ?>