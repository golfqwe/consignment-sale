<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>

<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">

    <?= $this->include('Layouts\navbar') ?>

    <?php if (!empty($errors)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errors as $field => $error): ?>
                <p><?= esc($error) ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <div class="w-full px-6 mx-auto">
        <div
            class="relative px-6 flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">


            <div class="pb-0 mb-0 mt-4 border-b border-solid shrink-0 border-slate-100 rounded-t-xl">
                <h6>รายละเอียดสัญญา</h6>
            </div>
            <div class="flex-auto p-4">
                <div class="flex flex-row space-x-4">
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1  text-xs text-slate-700">เลขที่สัญญา</label>
                        <div class="font-bold" id="contract_id">
                            <?php echo date_format(date_create(), "Ymd"); ?>C<?php echo str_pad($contract['id'] + 1, 8, "0", STR_PAD_LEFT); ?>
                        </div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">พยาน 1</label>
                        <div class="font-bold" id="witness1"><?php echo esc($contract['witness1']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">พยาน 2</label>
                        <div class="font-bold" id="witness2"><?php echo esc($contract['witness2']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">สถานที่เก็บ</label>
                        <div class="font-bold" id="storage_location"><?php echo esc($contract['storage_location']); ?>
                        </div>
                    </div>
                    <div class="basis-1/5"></div>
                </div>
                <div class="flex flex-row space-x-4 mt-4">
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">ยอดฝาก</label>
                        <div class="font-bold" id="deposit_amount">
                            <?php echo esc(number_format($contract['deposit_amount'], 2)); ?>
                        </div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">อัตราดอกเบี้ย</label>
                        <div class="font-bold" id="interest_rate"><?php echo esc($contract['interest_rate']); ?> %</div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">เงินต้น</label>
                        <div class="font-bold" id="total"><?php echo esc(number_format($contract['total'], 2)); ?>
                        </div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">วันที่ฝาก</label>
                        <div class="font-bold" id="deposit_date"><?php echo esc($contract['deposit_date']); ?> </div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">วันที่สิ้นสุดสัญญา</label>
                        <div class="font-bold" id="contract_end_date"><?php echo esc($contract['contract_end_date']); ?>
                        </div>
                    </div>
                </div>
                <form id="formEditContract">
                    <input type="hidden" name="conunt_date" value="<?php echo esc($contract['interest_day']); ?>" />
                    <input type="hidden" name="contract_id" value="<?php echo esc($contract['id']); ?>" />
                    <?php if ($contract['status'] == 'active') { ?>
                        <div class="flex flex-row space-x-4 mt-4">

                            <div class="basis-1/4">
                                <label class="mb-2 ml-1 text-xs text-slate-700">แก้ไข</label>
                                <div class="mb-4">
                                    <input type="radio" id="contactChoice1" name="contact" value="continue" checked />
                                    <label for="contactChoice1" class="mr-4">ต่อดอก</label>

                                    <input type="radio" id="contactChoice2" name="contact" value="redeem" />
                                    <label for="contactChoice2">ไถ่ถอน</label>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="flex flex-row space-x-4 ">
                        <?php if ($contract['status'] == 'active') { ?>
                            <div class="basis-1/5 hidden sectionContinue" id="">
                                <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ต่อดอกครั้งที่ </label>
                                <div class="mb-4 mr-4" id="countInterest"><?php echo esc(count($history) + 1); ?></div>
                            </div>
                            <div class="basis-1/5 hidden">
                            </div>
                            <div class="basis-1/5 hidden sectionContinue">
                                <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ดอกเบี้ย <span
                                        class="text-red-600">*</span></label>
                                <div class="mb-4">
                                    <input type="text" required name="inputInterest"
                                        value="<?php echo number_format((float) $contract['deposit_amount'] * (int) $contract['interest_rate'] / 100); ?>"
                                        class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow" />
                                </div>
                            </div>
                            <div class="basis-1/5 hidden sectionRedeem">
                                <label class="mb-2 ml-1 font-bold text-xs text-slate-700">เงินต้น <span
                                        class="text-red-600">*</span></label>
                                <div class="mb-4">
                                    <input type="text" readonly id="amount"
                                        value="<?php echo esc(number_format($contract['total'], 2)); ?>"
                                        class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow" />
                                </div>
                            </div>
                            <div class="basis-1/5">
                                <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค่าปรับ <span
                                        class="text-red-600">*</span></label>
                                <div class="mb-4">
                                    <input type="text" name="inputFine" value=""
                                        class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow" />
                                </div>
                            </div>
                            <div class="basis-1/5">
                                <label class="mb-2 ml-1 font-bold text-xs text-slate-700">รวม <span
                                        class="text-red-600">*</span></label>
                                <div class="mb-4">
                                    <input type="text" required name="inputTotal"
                                        value="<?php echo ((float) $contract['deposit_amount'] * (int) $contract['interest_rate'] / 100); ?>"
                                        class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow" />
                                </div>
                            </div>
                        <?php } ?>
                        <div class="basis-1/5">
                            <label class="mb-2 ml-1 font-bold text-xs text-slate-700"></label>
                            <div class="mb-4">
                            <?php if ($contract['status'] == 'active') { ?>
                                <button type="submit"
                                    class="inline-block px-6 py-3 mr-3 font-bold text-center text-white uppercase align-middle transition-all rounded-lg cursor-pointer bg-gradient-to-tl from-purple-700 to-pink-500 leading-pro text-xs ease-soft-in tracking-tight-soft shadow-soft-md bg-150 bg-x-25 hover:scale-102 active:opacity-85 hover:shadow-soft-xs">
                                    บันทึก
                                </button>
                                <?php } ?>
                                <a onclick="downloadPdf()"
                                    class="inline-block px-6 py-3 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                                    <i
                                        class="mr-2 fa fa-print bg-150 bg-gradient-to-tl from-blue-600 to-blue-400 bg-x-25 bg-clip-text"></i>
                                    พิมพ์</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>


            <div class="pb-0 mb-0 mt-4 border-b border-b-solid shrink-0 rounded-t-2xl border-slate-100 ">
                <h6>ข้อมูลผู้ขายฝาก</h6>
            </div>
            <div class="flex-auto px-4 ">
                <div class="flex flex-row space-x-4">
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">รหัสลูกค้า</label>
                        <div class="font-bold" id="customer_id"><?php echo esc($customer['id']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">ชื่อ - นามสกุล</label>
                        <div class="font-bold" id="full_name"><?php echo esc($customer['full_name']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">เลขบัตรประจำตัวประชาชน</label>
                        <div class="font-bold" id="personal_id"><?php echo esc($customer['personal_id']); ?></div>
                    </div>
                    <div class="basis-1/5"></div>
                    <div class="basis-1/5"></div>
                </div>
                <div class="flex flex-row space-x-4  mt-4">
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">บ้านเลขที่</label>
                        <div class="font-bold" id="house_no"><?php echo esc($customer['house_no']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">ซอย / ถนน</label>
                        <div class="font-bold" id="road"><?php echo esc($customer['road']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">ตำบล</label>
                        <div class="font-bold" id="sub_district"><?php echo esc($customer['sub_district']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">อำเภอ</label>
                        <div class="font-bold" id="district"><?php echo esc($customer['district']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">จังหวัด</label>
                        <div class="font-bold" id="province"><?php echo esc($customer['province']); ?></div>
                    </div>
                </div>
                <div class="flex flex-row space-x-4 mt-4">
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">รหัสไปรษณีย์</label>
                        <div class="font-bold" id="post_code"><?php echo esc($customer['post_code']); ?></div>
                    </div>
                    <div class="basis-1/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">เบอร์โทรศัพท์</label>
                        <div class="font-bold" id="telephone"><?php echo esc($customer['telephone']); ?></div>
                    </div>
                    <div class="basis-3/5">
                        <label class="mb-2 ml-1 text-xs text-slate-700">หมายเหตุ</label>
                        <div class="font-bold" id="mark"><?php echo esc($customer['mark']); ?></div>
                    </div>

                </div>
            </div>
            <div class="pb-0 mb-0 mt-6 bg-white  border-b-0 border-b-solid rounded-t-2xl border-b-transparent ">

                <div class="flex flex-wrap -mx-3">
                    <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                        <h6>ข้อมูลสินค้าขายฝาก</h6>
                    </div>
                </div>
            </div>
            <div class="flex-auto px-0 pt-0 pb-2">
                <div class="p-0 overflow-x-auto">
                    <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                        <thead class="align-bottom">
                            <tr>
                                <th
                                    class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ::</th>
                                <th
                                    class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    สินค้า</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ยี่ห้อ / รุ่น</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ขนาด</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    น้ำหนัก</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    สี</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    หมายเลขสินค้า</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    หมายเหตุ</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    รูป</th>
                                <th
                                    class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-gray-200 border-solid shadow-none tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $index = 1;
                            foreach ($products as $itemProduct):
                                $checkLastItem = count($products) == $index ? '-0' : ''
                                    ?>
                                <tr>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm"><?php echo $index; ?></p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc($itemProduct['name']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc($itemProduct['brand_model']); ?>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                                            <?php echo esc($itemProduct['size']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                                            <?php echo esc($itemProduct['weight']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                                            <?php echo esc($itemProduct['color']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                                            <?php echo esc($itemProduct['serial_number']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                                            <?php echo esc($itemProduct['mark']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <?php if ($itemProduct['image']) { ?>
                                            <img src="<?php echo esc($itemProduct['image']); ?>" alt="img-blur-shadow"
                                                width="125" class=" shadow-soft-2xl rounded-2xl" />
                                        <?php } else { ?>
                                            <p class="mb-0 font-semibold leading-tight text-sm text-center">-</p>
                                        <?php } ?>

                                    </td>
                                </tr>
                                <?php
                                $index++;
                            endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pb-0 mb-0 mt-4 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
                <h6>ประวัติต่อดอก</h6>
            </div>
            <div class="flex-auto px-0 mt-4 pb-2">
                <div class="p-0 overflow-x-auto">
                    <table id="history" class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                        <thead class="align-bottom">
                            <tr>
                                <th
                                    class="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ::</th>
                                <th
                                    class="px-6 py-3 pl-2 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    วันที่</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ดอกเบี้ย</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    ค่าปรับ</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    รวม</th>
                                <th
                                    class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                                    หมายเหตุ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $index = 1;
                            foreach ($history as $itemHistory):
                                $checkLastItem = count($history) == $index ? '-0' : ''
                                    ?>
                                <tr>
                                    <td
                                        class="px-6 align-middle text-left bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm"><?php echo $index; ?></p>
                                    </td>
                                    <td
                                        class="p-2 align-middle text-center bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc($itemHistory['created_at']); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle text-right bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc(number_format($itemHistory['amount_pay'], '2')); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle text-right bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc(number_format($itemHistory['fine'], '2')); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle text-right bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc(number_format((float) $itemHistory['fine'] + (float) $itemHistory['amount_pay'], '2')); ?>
                                        </p>
                                    </td>
                                    <td
                                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                                        <p class="mb-0 font-semibold leading-tight text-sm">
                                            <?php echo esc($itemHistory['mark']); ?>
                                        </p>
                                    </td>
                                </tr>
                                <?php
                                $index++;
                            endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</main>
<script src="<?php echo base_url() ?>/assets/js/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/pdfmake/th-sarabun.js"></script>

<script>
    let products = <?php echo json_encode($products); ?>;
    let history = <?php echo json_encode($history); ?>;

    let dataPrintout = {
        "contract_id": <?php echo $contract['id']; ?>,
        "witness1": "<?php echo $contract['witness1']; ?>",
        "witness2": "<?php echo $contract['witness2']; ?>",
        "storage_location": "<?php echo $contract['storage_location']; ?>",
        "deposit_amount": "<?php echo number_format($contract['deposit_amount'], 2); ?>",
        "interest_rate": "<?php echo $contract['interest_rate']; ?>",
        "total": "<?php echo number_format($contract['total'], 2); ?>",
        "deposit_date": "<?php echo $contract['deposit_date']; ?>",
        "contract_end_date": "<?php echo $contract['contract_end_date']; ?>",
        "custome": {
            "id": "<?php echo $customer['id']; ?>",
            "full_name": "<?php echo $customer['full_name']; ?>",
            "personal_id": "<?php echo $customer['personal_id']; ?>",
            "house_no": "<?php echo $customer['house_no']; ?>",
            "road": "<?php echo $customer['road']; ?>",
            "sub_district": "<?php echo $customer['sub_district']; ?>",
            "district": "<?php echo $customer['district']; ?>",
            "province": "<?php echo $customer['province']; ?>",
            "post_code": "<?php echo $customer['post_code']; ?>",
            "telephone": "<?php echo $customer['telephone']; ?>",
            "mark": "<?php echo $customer['mark']; ?>"
        },
        "interest_day": 30,
        "quantity": 1,
        "products": products,
        "history": history,
    };

    $(document).ready(function () {




        new Cleave('[name="inputInterest"]', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        new Cleave('[name="inputFine"]', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        // new Cleave('[name="inputInterest2"]', {
        //     numeral: true,
        //     numeralThousandsGroupStyle: 'thousand'
        // });
        // new Cleave('[name="discount_amount"]', {
        //     numeral: true,
        //     numeralThousandsGroupStyle: 'thousand'
        // });

        // let contract_end_date = new Date($('#contract_end_date').text());

        // let currentDate = new Date();

        // if (contract_end_date > currentDate) {
        //     alert('Given date is greater than the current date.');
        // } else {
        //     alert('Given date is not greater than the current date.');
        // }



        let chek = $('input[name="contact"]:checked').val();
        if (chek == 'continue') {
            $('.sectionContinue').removeClass('hidden');
        } else {
            $('.sectionRedeem').removeClass('hidden');
        }

        $('input[name="contact"]').change(function () { // bind a function to the change event
            if ($(this).is(":checked")) { // check if the radio is checked
                var val = $(this).val(); // retrieve the value
                if (val == 'continue') {
                    $('[name="inputTotal"]').val($('[name="inputInterest"]').val());
                    $('.sectionRedeem').addClass('hidden');
                    $('.sectionContinue').removeClass('hidden');
                } else {
                    $('[name="inputTotal"]').val($('#amount').val());
                    $('.sectionContinue').addClass('hidden');
                    $('.sectionRedeem').removeClass('hidden');
                }
            }
        });

        $('input[name="typeContact"]').change(function () { // bind a function to the change event
            if ($(this).is(":checked")) { // check if the radio is checked
                var val = $(this).val(); // retrieve the value
                if (val == 'inDue') {
                    $('#sectionOverdue').addClass('hidden');
                    $('#sectionInDue').removeClass('hidden');
                } else {
                    $('#sectionInDue').addClass('hidden');
                    $('#sectionOverdue').removeClass('hidden');
                }

            }
        });



        $('[name="inputFine"]').keyup(function (e) {
            let fine = e.currentTarget.value.replaceAll(',', '');


            let interest = $('[name="inputInterest"]').val().replaceAll(',', '')

            let result = Number(interest) + Number(fine);
            $('[name="inputTotal"]').val(new Intl.NumberFormat().format(result));
        });
        $('[name="inputInterest"]').keyup(function (e) {
            let interest = e.currentTarget.value.replaceAll(',', '');


            let fine = $('[name="inputFine"]').val().replaceAll(',', '')

            let result = Number(interest) + Number(fine);
            $('[name="inputTotal"]').val(new Intl.NumberFormat().format(result));
        });


        $("#formEditContract").submit(function (e) {
            e.preventDefault();
            let contract = $('#formEditContract').serializeJSON();
            console.log("🚀 ~ contract:", contract)
            let bodyContract = {}
            if (contract.contact === "continue") { //ต่อดอก

                var contractEndDate = new Date($('#contract_end_date').text());

                // add a day
                contractEndDate.setDate(contractEndDate.getDate() + Number(contract.conunt_date));
                bodyContract = {
                    contract_end_date: contractEndDate.toISOString().substring(0, 10),
                    amount_pay: contract.inputInterest.replaceAll(',', ''),
                    fine: contract.inputFine.replaceAll(',', ''),
                };

            } else {
                bodyContract = {
                    status: 'inActive'
                }
            }

            Swal.fire({
                title: "คุณต้องการบันทึกข้อมูลนี้ใช่หรื่อไม่?",
                showCancelButton: true,
                confirmButtonText: "บันทึก",
                denyButtonText: `ยกเลิก`
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {


                    $.ajax({
                        headers: { 'X-Requested-With': 'XMLHttpRequest' },
                        url: contract.contact === "continue" ? "/orders/continueContract" : "/orders/update",
                        type: "post",
                        data: JSON.stringify({
                            contract_id: $('[name="contract_id"]').val(),
                            ...bodyContract
                        }),
                        contentType: "application/json; charset=utf-8",
                        success: async function (response) {
                            await Swal.fire({
                                title: 'Success',
                                text: 'บันทึกข้อมูลสำเร็จ',
                                icon: 'success',
                            })
                            if (contract.contact === "continue") {
                                window.location.reload();
                            } else {
                                window.location.href = '/orders';
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                title: 'Error!',
                                text: 'มีบางอย่างผิดพลาด',
                                icon: 'error',
                                confirmButtonText: 'close'
                            })
                        }
                    });
                }
            });

        });
    });
</script>
<script src="<?php echo base_url() ?>/assets/js/page/printOutContractDetail.js" async></script>


<?= $this->endSection(); ?>