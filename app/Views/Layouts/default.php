<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="<?php echo base_url() ?>/assets/images/logo.png" />
  <title><?php echo config('CustomClass')->siteName; ?></title>

  <!-- Main Styling -->
  <link href="<?php echo base_url() ?>/assets/css/soft-ui-dashboard-tailwind.css?v=1.0.5" rel="stylesheet">

  <link href="<?php echo base_url() ?>/assets/fontawesome/css/fontawesome.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>/assets/fontawesome/css/all.css" rel="stylesheet" />


  <link href="<?php echo base_url() ?>/assets/css/app.css" rel="stylesheet" />


  <script src="<?php echo base_url() ?>/assets/js/plugins/choices.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/js/plugins/flatpickr.min.js"></script>

  <script src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/js/jquery.serializejson.js"></script>
  
  <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/js/cleave.min.js"></script>


</head>

<body class="m-0 font-sans text-base antialiased font-normal leading-default bg-gray-50 text-slate-500">
  <?php echo $this->renderSection('content') ?>
</body>


  <!-- plugin for charts  -->
  <script src="<?php echo base_url() ?>/assets/js/plugins/chartjs.min.js" async></script>
  <!-- plugin for scrollbar  -->
  <script src="<?php echo base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js" async></script>
  <!-- main script file  -->
  <script src="<?php echo base_url() ?>/assets/js/soft-ui-dashboard-tailwind.js?v=1.0.5" async></script>
  <script src="<?php echo base_url() ?>/assets/js/pagination.js" async></script>





</html>