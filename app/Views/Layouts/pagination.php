<?php $pager->setSurroundCount(2) ?>

<div class="flex flex-wrap -mx-3">
  <div class="flex-none w-full max-w-full px-3">
    <div
      class="relative flex min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border items-center justify-between p-4">
      <div class="flex-1 flex content-center justify-between sm:invisible">
        <!-- <span class="text-sm text-gray-700 ">
          Showing <span class="font-medium">1</span> to <span class="font-medium">10</span> of <span
            class="font-medium">97</span> results
          </> -->
      </div>

      <div class="flex-1 flex items-center justify-between">

        <nav class="relative z-0 inline-flex rounded-md shadow-sm space-x-2 " aria-label="Pagination">
          <?php if ($pager->hasPrevious()) { ?>
            <a href="<?php echo $pager->getFirst(); ?>" aria-label="<?php echo lang('Pager.first'); ?>"
              class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 rounded-lg">
              <span><?php echo lang('Pager.first'); ?></span>
            </a>
          <?php } ?>
          <!-- Numbered page links -->
          <?php foreach ($pager->links() as $link): ?>
            <a href="<?php echo $link['uri']; ?>"
              class="<?php echo $link['active'] ? 'bg-x-25 border-fuchsia-500 text-fuchsia-500 ' : 'bg-indigo-50'; ?> z-10  border-indigo-500 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium rounded-lg"> <?php echo $link['title']; ?></a>
          <?php endforeach ?>
          <!-- Next Page -->
          <?php if ($pager->hasNext()) : ?>
            <a href="<?php echo $pager->getNext(); ?>" aria-label="<?php echo lang('Pager.next'); ?>"
              class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 rounded-lg">
              <span><?php echo lang('Pager.next'); ?></span>
            </a>
            <?php endif ?>
        </nav>
      </div>
    </div>
  </div>
</div>