<!-- sidenav  -->
<aside
  class="max-w-62.5 ease-nav-brand z-990 fixed inset-y-0 my-4 ml-4 block w-full -translate-x-full flex-wrap items-center justify-between overflow-y-auto rounded-2xl border-0 bg-white p-0 antialiased shadow-none transition-transform duration-200 xl:left-0 xl:translate-x-0 xl:bg-transparent">
  <div class="h-19.5">
  <i class="absolute top-0 right-0 invisible p-4 opacity-50 cursor-pointer fas fa-times text-slate-400 xl:invisible" sidenav-close></i>
    <a class="block px-8 py-6 m-0 text-sm whitespace-nowrap text-slate-700" href="javascript:;">
      <img src="<?= base_url() ?>/assets/images/logo.png"
        class="inline h-full max-w-full transition-all duration-200 ease-nav-brand max-h-10" alt="main_logo" />
      <span
        class="ml-1 text-lg font-semibold transition-all duration-200 ease-nav-brand"><?= config('CustomClass')->siteName; ?></span>
    </a>
  </div>

  <hr class="h-px mt-0 bg-transparent bg-gradient-to-r from-transparent via-black/40 to-transparent" />

  <div class="items-center block w-auto max-h-screen overflow-auto grow basis-full">
    <ul class="flex flex-col pl-0 mb-0">

      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-chart-pie fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">Dashboard</span>
        </a>
      </li>

      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/contract">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-plus fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">สัญญาใหม่</span>
        </a>
      </li>

      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/orders">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-print fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">ไถ่ถอน / ต่อดอก</span>
        </a>
      </li>

      <li class="w-full mt-4">
        <h6 class="pl-6 ml-2 text-xs font-bold leading-tight uppercase opacity-60">รายงาน</h6>
      </li>
      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/reports">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-file-lines fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">สรุปยอดขาย</span>
        </a>
      </li>
      <!-- <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/report_summary">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-file-lines fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">รายงานสรุป</span>
        </a>
      </li> -->

      <li class="w-full mt-4">
        <h6 class="pl-6 ml-2 text-xs font-bold leading-tight uppercase opacity-60">จัดการข้อมูล</h6>
      </li>

      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/interest_rate">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-percent fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">อัตราดอกเบี้ย</span>
        </a>
      </li>

      <!-- <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/discount">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-calendar fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">อัตราส่วนลด</span>
        </a>
      </li> -->

      <li class="mt-0.5 w-full">
        <a class="py-2.7 text-sm ease-nav-brand my-0 mx-4 flex items-center whitespace-nowrap px-4 transition-colors"
          href="/customer">
          <div
            class="shadow-soft-2xl mr-2 flex h-8 w-8 items-center justify-center rounded-lg bg-white bg-center stroke-0 text-center xl:p-2.5">
            <i class="fa-solid fa-user fill-slate-800 "></i>
          </div>
          <span class="ml-1 duration-300 opacity-100 pointer-events-none ease-soft">ลูกค้า</span>
        </a>
      </li>
    </ul>
  </div>

</aside>