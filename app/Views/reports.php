<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>

<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">

  <?= $this->include('Layouts\navbar') ?>


  <div class="w-full px-6 py-6 mx-auto">

    <div class="flex flex-wrap -mx-3">
      <div class="flex-none w-full max-w-full px-3">
        <div
          class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">
          <div class=" flex-auto px-4">
            <form id="formSearch">
              <div class="flex flex-row space-x-4">
                <div class="basis-1/4">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค้นหาโดย</label>
                  <div class="mb-4">
                    <select id="searchBy" name="type" required
                      class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">

                      <option value="date">วันที่</option>
                      <option value="range">ช่วงวันที่</option>
                      <option value="month">เดือน</option>
                    </select>
                  </div>
                </div>

                <div class="basis-1/4">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค้นหา</label>
                  <div class="mb-4">
                    <input id="datePicker" name="date" required
                      class="focus:shadow-soft-primary-outline dark:bg-gray-950 dark:placeholder:text-white/80 dark:text-white/80 text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none"
                      type="text" placeholder="วันที่ฝาก" />
                  </div>
                </div>
                <div class="basis-1/4 mt-6">
                  <button type="submit"
                    class="mt-1 inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                    <i
                      class="mr-2 fa fa-search bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                    ค้นหา</button>
                </div>
              </div>
            </form>
          </div>
          <div class="p-6 pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
            <div class="flex flex-wrap -mx-3">
              <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                <h6>รายงานสุรปยอดขาย</h6>
              </div>
              <div class="flex-none w-1/2 max-w-full px-3 text-right">
                <a onclick="downloadPdf()" disabled
                  class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                  <i
                    class="mr-2 fa fa-print bg-150 bg-gradient-to-tl from-blue-600 to-blue-400 bg-x-25 bg-clip-text"></i>
                  พิมพ์</a>
              </div>
            </div>
          </div>
          <div class="flex-auto px-0 pt-0 pb-2">
            <div class="p-0 overflow-x-auto">
              <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                <thead class="align-bottom">
                  <tr>
                    <th
                      class="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ::</th>
                    <th
                      class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      วันที่ / เดือนที่</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ยอดจำนำ</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ยอดต่อดอก</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ยอดไถ่คืน</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ยอดดอกเบี้ย</th>

                  </tr>
                </thead>
                <tbody>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>


</main>
<script src="<?php echo base_url() ?>/assets/js/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/pdfmake/th-sarabun.js"></script>

<script src="<?php echo base_url() ?>/assets/js/page/reports.js"></script>

<script>
  $(document).ready(function () {
    $("#formSearch").submit(function (e) {
      e.preventDefault();
      $("table tbody").empty();
      $.ajax({
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        url: "/reports/findReport",
        type: "post",
        data: JSON.stringify({
          type: $('[name="type"]').find(":selected").val(),
          date: $('[name="date"]').val()
        }),
        contentType: "application/json; charset=utf-8",
        success: async function (response) {
          console.log("🚀 ~ response:",)
          let items = response.items
          items.forEach((it, index) => {
             
            index++;
            let markup = "";
            markup = "<tr>";
            markup += ` 
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                        <div class="flex px-4 py-1">${index}</div>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent">
                        <span
                          class="mb-0 font-semibold leading-tight text-sm">${new Date(it.deposit_date).toLocaleDateString('th-TH', {  dateStyle: "long"})}</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-semibold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(it.deposit_amount)} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-semibold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(it.amount_pay)} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-semibold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(it.redemption_amount)} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-semibold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format( Number(it.interest))} บาท</span>
                      </td>
                `
            markup += "</tr>";
            tableBody = $("table tbody");
            tableBody.append(markup);
          });

          let markup = "";
            markup = "<tr>";
            markup += ` 
                      <td></td>
                      <td
                        class="p-2 align-middle font-bold bg-transparent border-b-0 whitespace-nowrap shadow-transparent text-center" >
                        <span>รวม</span>
                     
                      </td>
                     
                      <td
                        class="p-2 align-middle bg-transparent border-b-0 whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-bold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(items.reduce((accumulator, currentValue) => accumulator + Number(currentValue.deposit_amount), 0))} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b-0 whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-bold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(items.reduce((accumulator, currentValue) => accumulator + Number(currentValue.amount_pay), 0))} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b-0 whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-bold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(items.reduce((accumulator, currentValue) => accumulator + Number(currentValue.redemption_amount), 0))} บาท</span>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b-0 whitespace-nowrap shadow-transparent text-right">
                        <span
                          class="mb-0 font-bold leading-tight text-sm ">${new Intl.NumberFormat('en-EN', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format( items.reduce((accumulator, currentValue) => accumulator + Number(currentValue.interest), 0))} บาท</span>
                      </td>
                `
            markup += "</tr>";
            tableBody = $("table tbody");
            tableBody.append(markup);


        },
        error: function (jqXHR, textStatus, errorThrown) {
          Swal.fire({
            title: 'Error!',
            text: 'มีบางอย่างผิดพลาด',
            icon: 'error',
            confirmButtonText: 'close'
          })
        }
      });
    })
  });
</script>


<?= $this->endSection() ?>