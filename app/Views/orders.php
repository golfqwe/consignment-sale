<?= $this->extend('Layouts\default') ?>

<?= $this->section('content') ?>

<?= $this->include('Layouts\sidebar') ?>

<main class="ease-soft-in-out xl:ml-68.5 relative h-full max-h-screen rounded-xl transition-all duration-200">

  <?= $this->include('Layouts\navbar') ?>

  <?php if (!empty($errors)): ?>
    <div class="alert alert-danger">
      <?php foreach ($errors as $field => $error): ?>
        <p><?= esc($error) ?></p>
      <?php endforeach ?>
    </div>
  <?php endif ?>

  <div class="w-full px-6 py-6 mx-auto">

    <div class="flex flex-wrap -mx-3">
      <div class="flex-none w-full max-w-full px-3">
        <div
          class="relative flex flex-col min-w-0 mb-6 break-words bg-white border-0 border-transparent border-solid shadow-soft-xl rounded-2xl bg-clip-border">
          <div class=" flex-auto px-4">
            <form method="get">
              <div class="flex flex-row space-x-4">
                <div class="basis-1/4">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค้นหาโดย</label>
                  <div class="mb-4">
                    <select name="type"
                      class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">

                      <option value="contract">เลขที่สัญญา</option>
                      <option value="pid">เลขประจำตัวประชาชน</option>
                    </select>
                  </div>
                </div>
                <div class="basis-1/4">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">ค้นหา</label>
                  <div class="mb-4">
                    <input type="text" name="txtSearch"
                      class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow"
                      placeholder="ค้นหา" aria-label="contractNo" aria-describedby="contractNo-addon" />
                  </div>
                </div>
                <div class="basis-1/4">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">สถานะ</label>
                  <div class="mb-4">
                    <select name="status"
                      class="focus:shadow-soft-primary-outline text-sm leading-5.6 ease-soft block w-full rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 transition-all focus:border-fuchsia-300 focus:outline-none focus:transition-shadow">
                      <option value=""></option>
                      <!-- <option value="missed">ขาดส่งดอกเบี้ย</option> -->
                      <option value="overdue">เกินกำหนด</option>
                    </select>
                  </div>
                </div>
                <div class="basis-1/5">
                  <label class="mb-2 ml-1 font-bold text-xs text-slate-700">วันที่ฝาก</label>
                  <div class="mb-4">
                    <input id="datePicker" name="dateDeposit"
                      class="focus:shadow-soft-primary-outline dark:bg-gray-950 dark:placeholder:text-white/80 dark:text-white/80 text-sm leading-5.6 ease-soft block w-full appearance-none rounded-lg border border-solid border-gray-300 bg-white bg-clip-padding px-3 py-2 font-normal text-gray-700 outline-none transition-all placeholder:text-gray-500 focus:border-fuchsia-300 focus:outline-none"
                      type="text" placeholder="วันที่ฝาก" />
                  </div>
                </div>
                <div class="basis-1/5 mt-6">
                  <button type="submit"
                    class="mt-1 inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                    <i
                      class="mr-2 fa fa-search bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                    ค้นหา</button>
                </div>
              </div>
            </form>
          </div>
          <div class="p-6 pb-0 mb-0 bg-white border-b-0 border-b-solid rounded-t-2xl border-b-transparent">
            <div class="flex flex-wrap -mx-3">
              <div class="flex items-center flex-none w-1/2 max-w-full px-3">
                <h6>ไถ่ถอน / ต่อดอก</h6>
              </div>
              <div class="flex-none w-1/2 max-w-full px-3 text-right">
                <a href="/contract"
                  class="inline-block px-8 py-2 mb-0 font-bold text-center uppercase align-middle transition-all bg-transparent border border-solid rounded-lg shadow-none cursor-pointer leading-pro ease-soft-in text-xs bg-150 active:opacity-85 hover:scale-102 tracking-tight-soft bg-x-25 border-fuchsia-500 text-fuchsia-500 hover:opacity-75">
                  <i
                    class="mr-2 far fa-plus bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>
                  เพิ่ม</a>
              </div>
            </div>
          </div>
          <div class="flex-auto px-0 pt-0 pb-2">
            <div class="p-0 overflow-x-auto">
              <table class="items-center w-full mb-0 align-top border-gray-200 text-slate-500">
                <thead class="align-bottom">
                  <tr>
                    <th
                      class="px-6 py-3 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ::</th>
                    <th
                      class="px-6 py-3 pl-2 font-bold text-left uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      เลขที่สัญญา</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      เลขบัตรประจำตัวประชาชนผู้ขายฝาก</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      ชื่อ-สกุล ผู้ขายฝาก</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      วันที่ขายฝาก</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      วันที่สิ้นสุด</th>
                    <th
                      class="px-6 py-3 font-bold text-center uppercase align-middle bg-transparent border-b border-gray-200 shadow-none text-sm border-b-solid tracking-none whitespace-nowrap text-slate-400 opacity-70">
                      สถานะ</th>
                    <th
                      class="px-6 py-3 font-semibold capitalize align-middle bg-transparent border-b border-gray-200 border-solid shadow-none tracking-none whitespace-nowrap text-slate-400 opacity-70">
                    </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $index = 1;
                  foreach ($items as $item):
                    $checkLastItem = count($items) == $index ? '-0' : '';
                    ?>
                    <tr>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <div class="flex px-4 py-1">
                          <a href="javascript:;" class="font-semibold leading-tight text-sm text-slate-400">
                            <?php echo (10 * ($page - 1)) + $index; ?></a>
                        </div>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm">
                          <?php echo date_format(date_create($item['deposit_date']), "Ymd"); ?>C<?php echo str_pad($item['id'], 8, "0", STR_PAD_LEFT); ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo $item['personal_id']; ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center"><?php echo $item['full_name']; ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo $item['deposit_date']; ?>
                        </p>
                      </td>
                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <p class="mb-0 font-semibold leading-tight text-sm text-center">
                          <?php echo $item['contract_end_date']; ?>
                        </p>
                      </td>
                      <td
                        class="p-2 leading-normal text-center align-middle bg-transparent border-b<?php echo $checkLastItem; ?> text-sm whitespace-nowrap shadow-transparent">
                        <span
                          class="<?php echo $item['status'] == 'active' ? 'bg-gradient-to-tl from-green-600 to-lime-400' : 'bg-red'; ?>  px-3.6 text-sm rounded-1.8 py-2.2 inline-block whitespace-nowrap text-center align-baseline font-bold uppercase leading-none text-white">
                          <?php
                          if ($item['status'] == 'active') {
                            echo 'เปิดอยู่';
                          } else if ($item['status'] == 'inActive') {
                            echo 'ปิดแล้ว';
                          }
                          ?>
                        </span>
                      </td>

                      <td
                        class="p-2 align-middle bg-transparent border-b<?php echo $checkLastItem; ?> whitespace-nowrap shadow-transparent">
                        <div class=" text-right">

                          <a href="/orderDetail/<?php echo $item['id']; ?>"
                            class="btn-detail relative z-10 inline-block px-4 py-3 mb-0 font-bold text-center text-transparent uppercase align-middle transition-all border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 bg-gradient-to-tl from-blue-600 to-blue-400 hover:scale-102 active:opacity-85 bg-x-25 bg-clip-text"
                            data-id="<?php echo $item['id']; ?>"><i
                              class="mr-2 far fa-eye bg-150 bg-gradient-to-tl from-blue-600 to-blue-400 bg-x-25 bg-clip-text"></i>
                            views
                          </a>

                          <a class="btn-delete relative z-10 inline-block px-4 py-3 mb-0 font-bold text-center text-transparent uppercase align-middle transition-all border-0 rounded-lg shadow-none cursor-pointer leading-pro text-sm ease-soft-in bg-150 bg-gradient-to-tl from-red-600 to-rose-400 hover:scale-102 active:opacity-85 bg-x-25 bg-clip-text"
                            data-id="<?php echo $item['id']; ?>"><i
                              class="mr-2 far fa-trash-alt bg-150 bg-gradient-to-tl from-red-600 to-rose-400 bg-x-25 bg-clip-text"></i>Delete</a>

                        </div>
                      </td>
                    </tr>
                    <?php
                    $index++;
                  endforeach ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?= $pager; ?>


  </div>



  <script type="text/javascript">
    (function () {

      const element1 = document.getElementById('datePicker');
      flatpickr(element1, {
        mode: "range",
        maxDate: "today",
      });


      let chek = $('input[name="contact"]:checked').val();
      if (chek == 'continue') {
        $('#sectionContinue').removeClass('hidden');
      } else {
        $('#sectionRedeem').removeClass('hidden');
      }

      $('input[name="contact"]').change(function () { // bind a function to the change event
        if ($(this).is(":checked")) { // check if the radio is checked
          var val = $(this).val(); // retrieve the value
          if (val == 'continue') {
            $('#sectionRedeem').addClass('hidden');
            $('#sectionContinue').removeClass('hidden');
          } else {
            $('#sectionContinue').addClass('hidden');
            $('#sectionRedeem').removeClass('hidden');
          }
        }
      });

      $('input[name="typeContact"]').change(function () { // bind a function to the change event
        if ($(this).is(":checked")) { // check if the radio is checked
          var val = $(this).val(); // retrieve the value
          if (val == 'inDue') {
            $('#sectionOverdue').addClass('hidden');
            $('#sectionInDue').removeClass('hidden');
          } else {
            $('#sectionInDue').addClass('hidden');
            $('#sectionOverdue').removeClass('hidden');
          }

        }
      });



      $('[name="inputFine"]').keyup(function (e) {
        let fine = e.currentTarget.value.replaceAll(',', '');


        let interest = $('[name="inputInterest"]').val().replaceAll(',', '')

        let result = Number(interest) + Number(fine);
        $('[name="inputTotal"]').val(new Intl.NumberFormat().format(result));
      });

      $('.btn-delete').on('click', function () {

        const contract_id = $(this).data('id');
        Swal.fire({
          title: "คุณต้องการลบข้อมูลนี้ใช่หรื่อไม่?",
          showCancelButton: true,
          confirmButtonText: "บันทึก",
          denyButtonText: `ยกเลิก`
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {

            $.ajax({
              headers: { 'X-Requested-With': 'XMLHttpRequest' },
              url: "/contract/deleteContract",
              type: "post",
              data: JSON.stringify({
                contract_id
              }),
              contentType: "application/json; charset=utf-8",
              success: async function (response) {
                await Swal.fire({
                  title: 'Success',
                  text: 'บันทึกข้อมูลสำเร็จ',
                  icon: 'success',
                })
                window.location.reload();

              },
              error: function (jqXHR, textStatus, errorThrown) {
                Swal.fire({
                  title: 'Error!',
                  text: 'มีบางอย่างผิดพลาด',
                  icon: 'error',
                  confirmButtonText: 'close'
                })
              }
            });
          }
        });
      })
    })();
  </script>



</main>




<?= $this->endSection() ?>