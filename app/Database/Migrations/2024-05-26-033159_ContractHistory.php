<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ContractHistory extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'contract_id' => [
                'type' => 'INT',
            ],
            'amount_pay' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],
            'fine' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],
            'mark' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('contract_history');
    }

    public function down()
    {
        $this->forge->dropTable('contract_history');
    }
}
