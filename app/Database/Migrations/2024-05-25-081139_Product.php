<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Product extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'contract_id' => [
                'type' => 'INT',
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'brand_model' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'size' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'weight' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],
            'color' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'serial_number' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'mark' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
            ],
            'image' => [
                'type' => 'TEXT',
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' => ['open', 'padding' ,'close'],
                'default' => 'open',
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('product');
    }

    public function down()
    {
        $this->forge->dropTable('product');
    }
}
