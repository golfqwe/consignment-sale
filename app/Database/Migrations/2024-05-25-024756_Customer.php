<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Customer extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'full_name' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'personal_id' => [
                'type' => 'INT',
            ],
            'house_no' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ],
            'road' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'sub_district' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'district' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'province' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'post_code' => [
                'type' => 'VARCHAR',
                'constraint' => 5,
            ],
            'telephone' => [
                'type' => 'VARCHAR',
                'constraint' => 10,
            ],
            'mark' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' => ['active', 'inActive'],
                'default' => 'active',
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('customer');
    }

    public function down()
    {
        $this->forge->dropTable('customer');
    }
}
