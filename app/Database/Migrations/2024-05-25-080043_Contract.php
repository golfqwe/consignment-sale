<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Contract extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'witness1' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'witness2' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'storage_location' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
            ],
            'deposit_date' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'quantity' => [
                'type' => 'INT',
                'default' => '0',
            ],
            'deposit_amount' => [
                'type' => 'INT',
                'default' => '0',
            ],
            'interest_day' => [
                'type' => 'INT',
                'default' => '0',
            ],
            'interest_rate' => [
                'type' => 'INT',
                'default' => '0',
            ],
            'fee' => [
                'type' => 'INT',
                'default' => '0',
            ],
            'total' => [
                'type' => 'DECIMAL',
                'default' => '10,2',
            ],
            'contract_end_date' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'customer_id' => [
                'type' => 'INT',
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' => ['active', 'inActive'],
                'default' => 'open',
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('contract');
    }

    public function down()
    {
        $this->forge->dropTable('contract');
    }
}
