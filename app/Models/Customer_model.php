<?php
namespace App\Models;

use CodeIgniter\Model;

class Customer_model extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'customer';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $insertID = 0;
    protected $returnType = 'array';
    protected $useSoftDeletes = false;
    protected $protectFields = true;
    protected $allowedFields = [
        'full_name',
        'personal_id',
        'house_no',
        'road',
        'sub_district',
        'district',
        'province',
        'post_code',
        'telephone',
        'mark',
        'status',
        'created_at',
        'updated_at',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat = 'datetime';
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    // Validation
    protected $validationRules = [
        'full_name' => [
            'label' => 'full_name',
            'rules' => 'required'
        ],
        'personal_id' => [
            'label' => 'personal_id',
            'rules' => 'required'
        ]
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert = [];
    protected $afterInsert = [];
    protected $beforeUpdate = [];
    protected $afterUpdate = [];
    protected $beforeFind = [];
    protected $afterFind = [];
    protected $beforeDelete = [];
    protected $afterDelete = [];


}
