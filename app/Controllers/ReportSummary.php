<?php

namespace App\Controllers;

class ReportSummary extends BaseController
{
    public function index()
    {

        try {
            $db = \Config\Database::connect();

            $pager = service('pager');

            $page = (int) ($this->request->getGet('page') ?? 1);
            $perPage = 10;
            $offset = ($page - 1) * $perPage;

            // Get the search keyword from the request
            $type = $this->request->getGet('type');
            $status = $this->request->getGet('status');
            $txtSearch = $this->request->getGet('txtSearch');


            $builder = $db->table('contract');
            $builder->select('contract.*, contract.status, customer.full_name, customer.personal_id');
            $builder->join('customer', 'customer.id = contract.customer_id', 'left');

            // Add search condition if a search keyword is provided
            if ($type == "contract") {
                $builder->groupStart()
                    ->like('contract.id', $txtSearch)
                    ->groupEnd();
            }
            if ($type == "pid") {
                $builder->groupStart()
                    ->like('customer.personal_id', $txtSearch)
                    ->groupEnd();
            }

            if ($status == "overdue") {
                $builder->groupStart()
                    ->where('DATEDIFF('.date("Y-m-d").', customer.contract_end_date)  > 0')
                    ->groupEnd();
            }

            $builder->orderBy('created_at', 'DESC');
         
       

            $total = $builder->countAllResults(false);

            $query = $builder->get($perPage, $offset)->getResultArray();

        


            // Call makeLinks() to make pagination links.
            $pager_links = $pager->makeLinks($page, $perPage, $total, 'my_pagination');

            $data['items'] = $query;
            $data['pager'] = $pager_links;
            $data['page'] = $page;

            return view('report_summary', $data);
        } catch (\Exception $e) {
            return view('report_summary', ['errors' => $e->getMessage()]);
        }
    }

}


