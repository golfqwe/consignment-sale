<?php

namespace App\Controllers;

class Contract extends BaseController
{
    public function index()
    {
        $interestRateModel = new \App\Models\InterestRate_model();
        $itemsinterestRate = $interestRateModel->findAll();

        $customerModel = new \App\Models\Customer_model();
        $itemsCustomer = $customerModel->findAll();


        $db = \Config\Database::connect();
        $builder = $db->table('contract');
        $builder->selectMax('id');
        $max_query = $builder->get()->getFirstRow();

        $data['maxid'] = $max_query->id | 0;
        $data['interestRate'] = $itemsinterestRate;
        $data['customers'] = $itemsCustomer;

        return view('contract', $data);
    }

    public function save()
    {
        $db = \Config\Database::connect();
        $db->transStart();

        $contractModel = new \App\Models\Contract_model();
        $customerModel = new \App\Models\Customer_model();
        $productModel = new \App\Models\Product_model();

        try {

            // Performs an update, since the primary key, 'id', is found.
            $customerInsertedId = 0;
            $customer = $customerModel->where('personal_id', $this->request->getJsonVar('custome.personal_id'))->first();
            // var_dump($customer);
            $dataCustomer = [
                'full_name' => $this->request->getJsonVar('custome.full_name'),
                'personal_id' => $this->request->getJsonVar('custome.personal_id'),
                'house_no' => $this->request->getJsonVar('custome.house_no'),
                'road' => $this->request->getJsonVar('custome.road'),
                'sub_district' => $this->request->getJsonVar('custome.sub_district'),
                'district' => $this->request->getJsonVar('custome.district'),
                'province' => $this->request->getJsonVar('custome.province'),
                'post_code' => $this->request->getJsonVar('custome.post_code'),
                'telephone' => $this->request->getJsonVar('custome.telephone'),
                'mark' => $this->request->getJsonVar('custome.mark'),
            ];
            if ($customer != null) {
                $dataCustomer['id'] = $customer['id'];
                $customerInsertedId = $customer['id'];
                $customerModel->save($dataCustomer);
            } else {
                $customerModel->insert($dataCustomer);
                $customerInsertedId = $customerModel->getInsertID();
            }


            // insert Contact
            $bodyContract = array(
                'id' => (int) $this->request->getJsonVar('contract_id'),
                'customer_id' => (int) $customerInsertedId,
                'contract_end_date' => $this->request->getJsonVar('contract_end_date'),
                'deposit_amount' => $this->request->getJsonVar('deposit_amount'),
                'deposit_date' => $this->request->getJsonVar('deposit_date'),
                'interest_day' => $this->request->getJsonVar('interest_day'),
                'interest_rate' => $this->request->getJsonVar('interest_rate'),
                'quantity' => $this->request->getJsonVar('quantity'),
                'storage_location' => $this->request->getJsonVar('storage_location'),
                'total' => (float) $this->request->getJsonVar('total'),
                'witness1' => $this->request->getJsonVar('witness1'),
                'witness2' => $this->request->getJsonVar('witness2'),
            );

            $contractId = $this->request->getJsonVar('contract_id');
            $contract = $contractModel->where('id', $contractId)->first();

       
            if ($contract != null) {
                $contractModel->save($bodyContract);
                $productModel->where('contract_id', $contractId)->delete();
            } else {
                $contractModel->insert($bodyContract);
                $contractId = $contractModel->getInsertID();
            }


            $bodyProduct = [];
            $products = $this->request->getJsonVar('products');

            $index = 0;
            foreach ($products as $k => $v) {
                $bodyProduct[$index] = (array) $v;
                $bodyProduct[$index]['contract_id'] = $contractId;
            }

            $productModel->insertBatch($bodyProduct);

            $db->transComplete();

            if ($db->transStatus() === FALSE) {
                // Transaction failed

                return $this->response->setStatusCode(500)->setBody($db->error());
            }

            $res['id'] = $customerInsertedId;
            return $this->response->setStatusCode(200)->setJSON($res);
        } catch (\Exception $e) {
            $db->transRollback();
            return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }



        // // Insert cash into user cash box
        // // $contractModel->insert($cash);

        // $this->db->transComplete();

        // if ($this->db->transStatus() === FALSE) {
        //     $this->db->transRollback();
        //     return false;
        // } else {
        //     $this->db->transCommit();
        //     return true;
        // }
    }

    public function deleteContract()
    {

        try {
            $db = \Config\Database::connect();
            $db->transStart();

            $contractModel = new \App\Models\Contract_model();
            $productModel = new \App\Models\Product_model();
            $contractHistoryModel = new \App\Models\ContractHistory_model();

            $contract_id = $this->request->getJsonVar('contract_id');

            $db->transComplete();

            $contractModel->delete($contract_id);

            $contractHistoryModel->where('contract_id', $contract_id)->delete();
            $productModel->where('contract_id', $contract_id)->delete();

            if ($db->transStatus() === FALSE) {
                // Transaction failed
                return $this->response->setStatusCode(500)->setBody($db->error());
            }

        } catch (\Exception $e) {
            $db->transRollback();
            return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }

    }

}
