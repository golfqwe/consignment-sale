<?php

namespace App\Controllers;

class Customer extends BaseController
{
    public function index()
    {
        $customerModel = new \App\Models\Customer_model();
        $items = $customerModel->findAll();
        $data['items'] = $items;

        return view('customer', $data);
    }

    public function save()
    {
        $customerModel = new \App\Models\Customer_model();
        $data = array(
            'full_name' => $this->request->getPost('full_name'),
            'personal_id' => $this->request->getPost('personal_id'),
            'house_no' => $this->request->getPost('house_no'),
            'road' => $this->request->getPost('road'),
            'sub_district' => $this->request->getPost('sub_district'),
            'district' => $this->request->getPost('district'),
            'province' => $this->request->getPost('province'),
            'post_code' => $this->request->getPost('post_code'),
            'telephone' => $this->request->getPost('telephone'),
            'mark' => $this->request->getPost('mark'),
        );
        if ($customerModel->save($data) === false) {
            return view('customer', ['errors' => $customerModel->errors()]);
        } else {
            return redirect()->to('customer');
        }
    }

    public function update()
    {
        $customerModel = new \App\Models\Customer_model();
        $id = $this->request->getPost('customer_id');
        $data = array(
            'full_name' => $this->request->getPost('full_name'),
            'personal_id' => $this->request->getPost('personal_id'),
            'house_no' => $this->request->getPost('house_no'),
            'road' => $this->request->getPost('road'),
            'sub_district' => $this->request->getPost('sub_district'),
            'district' => $this->request->getPost('district'),
            'province' => $this->request->getPost('province'),
            'post_code' => $this->request->getPost('post_code'),
            'telephone' => $this->request->getPost('telephone'),
            'mark' => $this->request->getPost('mark'),
        );
        
        if ($customerModel->update($id, $data) === false) {
            return view('customer', ['errors' => $customerModel->errors()]);
        } else {
            return redirect()->to('customer');
        }
    }
}
