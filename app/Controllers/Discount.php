<?php

namespace App\Controllers;

class Discount extends BaseController
{
    public function index()
    {
        $discountModel = new \App\Models\Discount_model();
        $items = $discountModel->findAll();
        $data['items'] = $items;

        return view('discount', $data);
    }

    public function save()
    {
        $discountModel = new \App\Models\Discount_model();
        $data = array(
            'conunt_date' => $this->request->getPost('countDate'),
            'percent' => $this->request->getPost('percent'),
        );
        if ($discountModel->save($data) === false) {
            return view('discount', ['errors' => $discountModel->errors()]);
        } else {
            return redirect()->to('discount');
        }
    }

    public function update()
    {
        $discountModel = new \App\Models\Discount_model();
        $id = $this->request->getPost('discount_id');
        $data = array(
            'conunt_date' => $this->request->getPost('countDate'),
            'percent' => $this->request->getPost('percent')
        );
        if ($discountModel->update($id, $data) === false) {
            return view('discount', ['errors' => $discountModel->errors()]);
        } else {
            return redirect()->to('discount');
        }
    }

    public function delete()
    {
        $discountModel = new \App\Models\Discount_model();
        $id = $this->request->getPost('discount_id');

        if ($discountModel->delete($id) === false) {
            return view('discount', ['errors' => $discountModel->errors()]);
        } else {
            return redirect()->to('discount');
        }
    }
}
