<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    public function index()
    {


        return view('dashboard');
    }

    public function getDashboard()
    {

        try {
            $contractModel = new \App\Models\Contract_model();
            $customerModel = new \App\Models\Customer_model();
            $contractHistoryModel = new \App\Models\ContractHistory_model();
            $db = \Config\Database::connect();


            $data['deposit'] = $contractModel->where('deposit_date', date("Y-m-d"))->selectSum('deposit_amount')->first();


            
            $data['interest'] = $contractHistoryModel->where('SUBSTRING(created_at,1,10)', date("Y-m-d"))->selectSum('fine')->selectSum('amount_pay')->first();
            
            $countContract = $contractModel->selectCount('id')->first();

            $data['count_contract'] = $countContract['id'];

            $countCustomer = $customerModel->selectCount('id')->first();
            $data['count_customer'] = $countCustomer['id'];


            $sql = "SELECT `deposit_date`, (`total`) FROM `contract` GROUP by `deposit_date`";
            $query = $db->query($sql);
            $data['itemsGraph'] = $query->getResultArray();

    
            // echo var_dump($data);
            return $this->response->setStatusCode(200)->setJSON($data);

        } catch (\Exception $e) {
            return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }
    }
}
