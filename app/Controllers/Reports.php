<?php

namespace App\Controllers;

class Reports extends BaseController
{
    public function index()
    {



        // var_dump($query->getErrorCode());

        // Return data as JSON or render a view with data
        // return $this->response->setJSON($data);
        return view('reports');




    }
    public function findReport($type = 'date', $val = '')
    {
        try {
            $db = \Config\Database::connect();

            $type = $this->request->getJsonVar('type');

            $valDate = $this->request->getJsonVar('date');
            $where = "";
            $where2 = "";
            switch ($type) {
                case 'date':
                    $where = "deposit_date = '$valDate'";
                    $where2 = "c2.updated_at = '$valDate'";
                    break;
                case 'range':
                    $dateSearch = explode(" to ", $valDate);
                    $where = "deposit_date BETWEEN '$dateSearch[0]' AND '$dateSearch[1]' ";
                    $where2 = "c2.updated_at BETWEEN '$dateSearch[0]' AND '$dateSearch[1]' ";
                    break;
                case 'month':
                    $dateSearch = explode("-", $valDate);
                    $where = " MONTH(deposit_date) = $dateSearch[1] AND YEAR(deposit_date) = $dateSearch[0] ";
                    $where2 = " MONTH(c2.updated_at) = $dateSearch[1] AND YEAR(c2.updated_at) = $dateSearch[0] ";
                    break;
            }

            $sql = "SELECT deposit_date, 
                    SUM(deposit_amount) deposit_amount,
                    SUM((deposit_amount * interest_rate) /100)  interest,
                    SUM(ch.amount_pay + ch.fine) amount_pay,
                    (
                        select sum(total) total FROM contract c2 where c2.deposit_date = c.deposit_date  and c2.status = 'inActive' 
                    ) redemption_amount


                FROM contract c 
                LEFT JOIN contract_history ch ON ch.contract_id = c.id
                WHERE $where 
                GROUP BY deposit_date";


            // var_dump($sql);
            $data['items'] = [];
            if ($type) {
                $query = $db->query($sql);
                $data['items'] = $query->getResultArray();
            }

            return $this->response->setJSON($data);
        } catch (\Exception $e) {
            return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }



    }
}
