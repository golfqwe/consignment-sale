<?php

namespace App\Controllers;

class InterestRate extends BaseController
{

    
    public function index()
    {
        $interestRateModel = new \App\Models\InterestRate_model();
        $items = $interestRateModel->findAll();
        $data['items'] = $items;

        return view('interest_rate', $data);
    }

    public function save()
    {
        $interestRateModel = new \App\Models\InterestRate_model();
        $data = array(
            'conunt_date' => $this->request->getPost('countDate'),
            'percent' => $this->request->getPost('percent'),
        );
        if ($interestRateModel->save($data) === false) {
            return view('interest_rate', ['errors' => $interestRateModel->errors()]);
        } else {
            return redirect()->to('interest_rate');
        }
    }

    public function update()
    {
        $interestRateModel = new \App\Models\InterestRate_model();
        $id = $this->request->getPost('interest_rate_id');
        $data = array(
            'conunt_date' => $this->request->getPost('countDate'),
            'percent' => $this->request->getPost('percent')
        );
        if ($interestRateModel->update($id, $data) === false) {
            return view('interest_rate', ['errors' => $interestRateModel->errors()]);
        } else {
            return redirect()->to('interest_rate');
        }
    }

    public function delete()
    {
        $interestRateModel = new \App\Models\InterestRate_model();
        $id = $this->request->getPost('interest_rate_id');

        if ($interestRateModel->delete($id) === false) {
            return view('interest_rate', ['errors' => $interestRateModel->errors()]);
        } else {
            return redirect()->to('interest_rate');
        }
    }
}
