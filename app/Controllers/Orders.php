<?php

namespace App\Controllers;

class Orders extends BaseController
{
    public function index()
    {

        try {
            $db = \Config\Database::connect();

            $pager = service('pager');

            $page = (int) ($this->request->getGet('page') ?? 1);
            $perPage = 10;
            $offset = ($page - 1) * $perPage;

            // Get the search keyword from the request
            $type = $this->request->getGet('type');
            $status = $this->request->getGet('status');
            $txtSearch = $this->request->getGet('txtSearch');
            $dateDeposit = $this->request->getGet('dateDeposit');


            $builder = $db->table('contract');
            $builder->select('contract.*, contract.status, customer.full_name, customer.personal_id');
            $builder->join('customer', 'customer.id = contract.customer_id', 'left');

            // Add search condition if a search keyword is provided
            if ($type == "contract" && $txtSearch != "") {
                $builder->groupStart()
                    ->like('contract.id', $txtSearch)
                    ->groupEnd();
            }
            if ($type == "pid" && $txtSearch != "") {
                $builder->groupStart()
                    ->like('customer.personal_id', $txtSearch)
                    ->groupEnd();
            }

            if ($status == "overdue") {
                $builder->groupStart()
                    ->where('DATEDIFF("' . date("Y-m-d") . '", contract_end_date)  > 0 and contract.status = "active"')
                    ->groupEnd();
            }

            if ($dateDeposit != "") {
                $dateSearch = explode(" to ", $dateDeposit);
                $where = "deposit_date BETWEEN '$dateSearch[0]' AND '$dateSearch[1]' ";
                $builder->groupStart()
                    ->where($where)
                    ->groupEnd();
            }


            $builder->orderBy('created_at', 'DESC');
            $total = $builder->countAllResults(false);
        
            $query = $builder->get($perPage, $offset)->getResultArray();
 

            // Call makeLinks() to make pagination links.
            $pager_links = $pager->makeLinks($page, $perPage, $total, 'my_pagination');

            $data['items'] = $query;
            $data['pager'] = $pager_links;
            $data['page'] = $page;

            return view('orders', $data);
        } catch (\Exception $e) {
            return view('orders', ['errors' => $e->getMessage()]);
        }
    }



    public function getContract($id)
    {
        try {
            $contractModel = new \App\Models\Contract_model();
            $customerModel = new \App\Models\Customer_model();
            $productModel = new \App\Models\Product_model();
            $contractHistoryModel = new \App\Models\ContractHistory_model();

            $contract = $contractModel->find($id);

            $customer = $customerModel->find($contract['customer_id']);

            $history = $contractHistoryModel->where('contract_id', $id)->findAll();

            $products = $productModel->where('contract_id', $id)->findAll();


            $data['contract'] = $contract;
            $data['customer'] = $customer;
            $data['history'] = $history;
            $data['products'] = $products;
            // return $this->response->setStatusCode(200)->setJSON($data);
            return view('orderDetail', $data);
        } catch (\Exception $e) {
            return view('orderDetail', ['errors' => $e->getMessage()]);
            // return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }

    }

    public function update()
    {
        $contractModel = new \App\Models\Contract_model();
        $id = $this->request->getJsonVar('contract_id');
        $data = array(
            'status' => $this->request->getJsonVar('status'),
        );
        if ($contractModel->update($id, $data) === false) {
            return view('customer', ['errors' => $contractModel->errors()]);
        } else {
            return redirect()->to('customer');
        }

    }

    public function continueContract()
    {
        $db = \Config\Database::connect();
        $db->transStart();

        try {
            $contractHistoryModel = new \App\Models\ContractHistory_model();
            $contractModel = new \App\Models\Contract_model();
            $id = $this->request->getJsonVar('contract_id');

            $bodyContract = array(
                'contract_end_date' => $this->request->getJsonVar('contract_end_date'),
            );


            $contractModel->update($id, $bodyContract);

            $bodyContractHistory = [
                "contract_id" => (int) $id,
                "amount_pay" => (double) $this->request->getJsonVar('amount_pay'),
                "fine" => (double) $this->request->getJsonVar('fine'),
                "mark" => "ต่อดอก "
            ];
            echo var_dump($bodyContractHistory);

            $contractHistoryModel->insert($bodyContractHistory);


            $db->transComplete();

            if ($db->transStatus() === FALSE) {
                // Transaction failed
                return $this->response->setStatusCode(500)->setBody('Transaction failed');
            }


            return $this->response->setStatusCode(200)->setBody('Transaction successful');
        } catch (\Exception $e) {
            $db->transRollback();
            return $this->response->setStatusCode(500)->setBody('Exception: ' . $e->getMessage());
        }

    }
}


