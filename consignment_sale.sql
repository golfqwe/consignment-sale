-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2024 at 09:44 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `consignment_sale`
--

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE `contract` (
  `id` int(10) UNSIGNED NOT NULL,
  `witness1` varchar(100) NOT NULL,
  `witness2` varchar(100) NOT NULL,
  `storage_location` varchar(200) NOT NULL,
  `deposit_date` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `deposit_amount` int(11) NOT NULL DEFAULT 0,
  `interest_day` int(11) NOT NULL,
  `interest_rate` int(11) NOT NULL DEFAULT 0,
  `fee` int(11) NOT NULL DEFAULT 0,
  `total` decimal(11,2) NOT NULL DEFAULT 0.00,
  `contract_end_date` varchar(100) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` enum('active','inActive') NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contract_history`
--

CREATE TABLE `contract_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `contract_id` int(11) NOT NULL,
  `fine` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_pay` decimal(10,2) NOT NULL,
  `mark` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `personal_id` int(13) NOT NULL,
  `house_no` varchar(50) NOT NULL,
  `road` varchar(100) NOT NULL,
  `sub_district` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `post_code` varchar(5) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `mark` varchar(100) NOT NULL,
  `status` enum('active','inActive') NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `full_name`, `personal_id`, `house_no`, `road`, `sub_district`, `district`, `province`, `post_code`, `telephone`, `mark`, `status`, `created_at`, `updated_at`) VALUES
(33, 'นายทดสอบ ระบบ', 2147483647, '123/456', 'เจริญ', 'นาดี', 'กมลาไสย', 'ขอนแก่น', '12345', '984-603-38', '-', 'active', '2024-05-30 22:33:26', '2024-05-31 02:01:44');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(10) UNSIGNED NOT NULL,
  `conunt_date` int(11) NOT NULL DEFAULT 0,
  `percent` int(11) NOT NULL DEFAULT 0,
  `status` enum('active','inActive') NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `conunt_date`, `percent`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 100, 'active', '2024-05-25 00:20:10', '2024-05-25 01:56:45'),
(2, 1, 10, 'active', '2024-05-25 00:20:26', '2024-05-25 02:03:17'),
(3, 30, 25, 'active', '2024-05-25 01:25:54', '2024-05-25 01:56:37'),
(4, 30, 25, 'active', '2024-05-25 01:26:01', '2024-05-25 01:54:42');

-- --------------------------------------------------------

--
-- Table structure for table `interest_rate`
--

CREATE TABLE `interest_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `conunt_date` int(11) NOT NULL DEFAULT 0,
  `percent` int(11) NOT NULL DEFAULT 0,
  `status` enum('active','inActive') NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interest_rate`
--

INSERT INTO `interest_rate` (`id`, `conunt_date`, `percent`, `status`, `created_at`, `updated_at`) VALUES
(1, 30, 10, 'active', '2024-05-25 02:22:00', '2024-05-25 22:52:15'),
(3, 90, 12, 'active', '2024-05-25 04:07:08', '2024-05-25 22:52:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(6, '2024-05-25-024218', 'App\\Database\\Migrations\\InterestRate', 'default', 'App', 1716625623, 1),
(7, '2024-05-25-024644', 'App\\Database\\Migrations\\Discount', 'default', 'App', 1716625623, 1),
(8, '2024-05-25-024756', 'App\\Database\\Migrations\\Customer', 'default', 'App', 1716625623, 1),
(9, '2024-05-25-080043', 'App\\Database\\Migrations\\Contract', 'default', 'App', 1716625623, 1),
(10, '2024-05-25-081139', 'App\\Database\\Migrations\\Product', 'default', 'App', 1716625623, 1),
(11, '2024-05-26-033159', 'App\\Database\\Migrations\\ContractHistory', 'default', 'App', 1716867692, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `contract_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `brand_model` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `weight` decimal(10,2) NOT NULL DEFAULT 0.00,
  `color` varchar(100) NOT NULL,
  `serial_number` varchar(100) NOT NULL,
  `mark` varchar(200) NOT NULL,
  `image` text NOT NULL,
  `status` enum('active','inActive') NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_history`
--
ALTER TABLE `contract_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_rate`
--
ALTER TABLE `interest_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contract`
--
ALTER TABLE `contract`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `contract_history`
--
ALTER TABLE `contract_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `interest_rate`
--
ALTER TABLE `interest_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
